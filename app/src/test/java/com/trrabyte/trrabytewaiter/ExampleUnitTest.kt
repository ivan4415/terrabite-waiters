package com.trrabyte.trrabytewaiter

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun subStringPrint(){
        var test = "Coca cola"

        while (test.length>=9){
            println("${test.substring(0,9).trim()}")
            test = test.substring(9,test.length)
        }
        println(test)

        var test2 = 2.2

        println(String.format("%.3f",test2))
    }
}