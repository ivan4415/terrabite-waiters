package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName

data class GetCheckResponse (

    @SerializedName("order")
    var order : OrderDetailResponse,

    @SerializedName("paymentMethods")
    var paymentMethods : ArrayList<PaymentMethodsResponse> = arrayListOf()
)

data class PaymentMethodsResponse(
    @SerializedName("id")
    var id : String,

    @SerializedName("description")
    var description : String
)