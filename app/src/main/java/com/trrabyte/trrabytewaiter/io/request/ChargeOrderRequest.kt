package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

class ChargeOrderRequest {

    @SerializedName("orderId")
    var orderId : Int = -1

    @SerializedName("paymentMethodId")
    var paymentMethodId : String = ""

    @SerializedName("branchId")
    var branchId : Int? = -1
}