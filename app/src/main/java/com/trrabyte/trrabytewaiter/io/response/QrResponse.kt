package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse

data class QrResponse(

    @SerializedName("state")
    var state : String = "",

    @SerializedName("folio")
    var folio : Int? = null,

    @SerializedName("tableNum")
    var tableNum : Int? = null,

    @SerializedName("order")
    var order : Order? = null

) : NewBaseResponse()

data class Order (
    @SerializedName("folio")
    var folio : Int? = null,

    @SerializedName("tableNumber")
    var tableNumber : Int? = 1
        ) : NewBaseResponse()