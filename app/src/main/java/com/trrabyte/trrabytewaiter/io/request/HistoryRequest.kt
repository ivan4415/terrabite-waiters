package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

class HistoryRequest {

    @SerializedName("date")
    var date : String = ""

    @SerializedName("page")
    var page : Int = 0

    @SerializedName("branchId")
    var branchId : Int = -1
}