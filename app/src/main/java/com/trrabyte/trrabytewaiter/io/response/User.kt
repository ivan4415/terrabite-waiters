package com.trrabyte.trrabytewaiter.io.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse

@Entity(tableName = "user")
class User : NewBaseResponse() {

        @PrimaryKey(autoGenerate = true)
        var roomId : Int = 0

        @SerializedName("branch")
        @Ignore
        var branch : Branch = Branch()

        @SerializedName("waiter")
        @Ignore
        var waiter : NewBaseResponse? = null

        @SerializedName("firstName")
        @ColumnInfo(name = "firstName")
        var firstName : String? = null

        @SerializedName("lastName")
        @ColumnInfo(name = "lastName")
        var lastName : String? = null

        @SerializedName("userRoleList")
        @Ignore
        var userRoleList : ArrayList<UserRole> = arrayListOf()
}