package com.trrabyte.trrabytewaiter.io.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse

@Entity(tableName = "branch")
class Branch() {

    @SerializedName("waiterId")
    @ColumnInfo(name = "waiterId")
    var waiterId: Int = -1

    @SerializedName("brand")
    @Ignore
    var brand: String? = null

    @SerializedName("subDomain")
    @Ignore
    var subDomain: String? = null

    @SerializedName("numTables")
    @Ignore
    var numTables: Int? = null

    @SerializedName("offset")
    @Ignore
    var offset: String? = null

    @SerializedName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey
    var id: Int? = null

    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String? = ""


}