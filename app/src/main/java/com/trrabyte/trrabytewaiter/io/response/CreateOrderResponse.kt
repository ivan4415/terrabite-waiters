package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse

data class CreateOrderResponse(
    @SerializedName("order")
    var order : OrderDetailResponse
)

data class OrderDetailResponse (
    @SerializedName("orderTypeId")
    var orderTyperId : String? = null,

    @SerializedName("orderStatusId")
    var orderStatusId : String? = null,

    @SerializedName("paymentMethodId")
    var paymentMethodId : String? = null,

    @SerializedName("folio")
    var folio : Int? = -1,

    @SerializedName("tableNumber")
    var tableNumber : Int? = -1,

    @SerializedName("totalAmount")
    var totalAmount : Double? = -1.0,

    @SerializedName("orderDate")
    var orderDate : String? = null,

    @SerializedName("orderDetailList")
    var orderDetailList : String? = null


        ) : NewBaseResponse()