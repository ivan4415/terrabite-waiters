package com.trrabyte.trrabytewaiter.io

import androidx.room.Room
import com.trrabyte.trrabytewaiter.base.io.ApplicationBase
import com.trrabyte.trrabytewaiter.io.dao.WaitersDataBase

class BadApplication : ApplicationBase() {

    lateinit var controllerWaiters : ControllerWaiters
    lateinit var db : WaitersDataBase
    companion object{
        fun getInstance( ) : BadApplication{
            return ApplicationBase.getInstance() as BadApplication
        }


    }
    override fun initApplication() {
        super.initApplication()
        controllerWaiters = ControllerWaiters(ctx)
        createDataBase()
        testProtected()
    }

    override fun createDataBase() {
        super.createDataBase()
        db = Room.databaseBuilder(getInstance(),WaitersDataBase::class.java,"waiters-db").allowMainThreadQueries().build()
    }
}