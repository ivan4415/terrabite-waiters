package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

class RecoveryRequest{

   @SerializedName("email")
   var email : String = ""
}

class VerifyCodeRequest{

    @SerializedName("code")
    var code : String = ""
}

class ResetPasswordRequest{

    @SerializedName("userId")
    var userId : Int = -1

    @SerializedName("password")
    var password : String = ""

    @SerializedName("confirmPassword")
    var confirm : String = ""
}