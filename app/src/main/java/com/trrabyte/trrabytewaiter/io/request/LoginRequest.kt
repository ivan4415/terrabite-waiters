package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.BaseRequest
import com.trrabyte.trrabytewaiter.base.io.NewBaseRequest

class LoginRequest: NewBaseRequest(){

    @SerializedName("email")
    var email : String? = null

    @SerializedName("password")
    var password : String? = null
}