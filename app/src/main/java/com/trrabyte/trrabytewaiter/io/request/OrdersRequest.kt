package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

 class OrdersRequest {

     @SerializedName("branchId")
     var branchId = -1

     @SerializedName("orderId")
     var orderId : Int? = null
 }