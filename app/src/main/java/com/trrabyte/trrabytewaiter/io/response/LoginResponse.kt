package com.trrabyte.trrabytewaiter.io.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse

data class LoginResponse (

    @SerializedName("token")
    var token : String,

    @SerializedName("branchList")
    var branchList : ArrayList<Branch>,

    @SerializedName("credential")
    var credential : Credential
)

data class Credential(

    @SerializedName("user")
    var user : User
) : NewBaseResponse()

data class UserRole(
    @SerializedName("role")
    var role : Role,
    @SerializedName("userId")
    var userId : Int? = null
) : NewBaseResponse()

data class Role(
    @SerializedName("description")
    var description : String? = null
):NewBaseResponse()