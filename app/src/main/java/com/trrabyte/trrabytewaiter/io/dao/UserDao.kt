package com.trrabyte.trrabytewaiter.io.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trrabyte.trrabytewaiter.io.response.Branch
import com.trrabyte.trrabytewaiter.io.response.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user LIMIT 1")
    fun getUser() : User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert (branch : User)

    @Query("DELETE FROM user")
    fun deleteAll()
}