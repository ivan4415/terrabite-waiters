package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import com.trrabyte.trrabytewaiter.io.response.Dish

class CreateOrderRequest : NewBaseResponse() {

    @SerializedName("orderTypeId")
    var orderTypeId = "eat_here"

    @SerializedName("folio")
    var folio = -1

    @SerializedName("branchId")
    var branchId = -1

    @SerializedName("tableNumber")
    var tableNumber = -12

    @SerializedName("orderDetailList")
    var orderDetailList = ArrayList<Dish>()

    @SerializedName("deletedDetailList")
    var deletedDetailList : ArrayList<Int>? = null
}