package com.trrabyte.trrabytewaiter.io

import android.content.Context
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.io.ControllerBase
import com.trrabyte.trrabytewaiter.io.request.*
import com.trrabyte.trrabytewaiter.io.response.*
import retrofit2.Call

//class ControllerWaiters(ctx : Context) : ControllerBase(ctx,"https://sandbox.trrabite.com/waiterservice/"), WaitersApi{
class ControllerWaiters(ctx : Context) : ControllerBase(ctx,"https://trrabite.com/waiterservice"), WaitersApi{

    private val serviceInterface : WaitersApi = retrofit.create(WaitersApi::class.java)

    override fun doLogin(loginRequest: LoginRequest): Call<BaseResponse<LoginResponse>> = serviceInterface.doLogin(loginRequest)

    override fun getOrders(ordersRequest: OrdersRequest): Call<BaseResponse<OrdersResponse>> = serviceInterface.getOrders(ordersRequest)

    override fun newOrder(ordersRequest: OrdersRequest): Call<BaseResponse<NewOrderResponse>> = serviceInterface.newOrder(ordersRequest)

    override fun createOrder(createOrdersRequest: CreateOrderRequest): Call<BaseResponse<CreateOrderResponse>> = serviceInterface.createOrder(createOrdersRequest)

    override fun getEditOrder(id: Int): Call<BaseResponse<EditOrderResponse>> = serviceInterface.getEditOrder(id)

    override fun updateOrder(updateOrder: CreateOrderRequest): Call<BaseResponse<EditOrderResponse>> = serviceInterface.updateOrder(updateOrder)

    override fun getCharge(id: Int): Call<BaseResponse<GetCheckResponse>> = serviceInterface.getCharge(id)

    override fun cancelOrder(order: OrdersRequest): Call<BaseResponse<String>> = serviceInterface.cancelOrder(order)

    override fun chargeOrder(chargeOrderRequest: ChargeOrderRequest): Call<BaseResponse<String>> = serviceInterface.chargeOrder(chargeOrderRequest)

    override fun sendEmail(email: RecoveryRequest): Call<BaseResponse<String>> = serviceInterface.sendEmail(email)

    override fun verifyCode(code: VerifyCodeRequest): Call<BaseResponse<UserRole>> = serviceInterface.verifyCode(code)

    override fun resetPassword(resetPassword: ResetPasswordRequest): Call<BaseResponse<String>> = serviceInterface.resetPassword(resetPassword)

    override fun getStadistics(stadisticRequest: StadisticsRequest): Call<BaseResponse<StadisticsResponse>> = serviceInterface.getStadistics(stadisticRequest)

    override fun getHistory(requestHistory: HistoryRequest): Call<BaseResponse<OrdersResponse>> = serviceInterface.getHistory(requestHistory)

    override fun getQrOrder(tableNum: Int, branchId: Int): Call<BaseResponse<QrResponse>> = serviceInterface.getQrOrder(tableNum, branchId)

}