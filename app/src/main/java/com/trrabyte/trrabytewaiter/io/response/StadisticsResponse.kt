package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName

data class StadisticsResponse (

    @SerializedName("branchList")
    var branchList : ArrayList<Branch>?,

    @SerializedName("bestTable")
    var bestTable : String?,

    @SerializedName("bestPromotion")
    var bestPromotion : String?,

    @SerializedName("ordersCount")
    var ordersCount : Int?,

    @SerializedName("ordersTotalAmount")
    var ordersTotalAmount : Double?,

    @SerializedName("promotionsAmount")
    var promotionsAmount : Double?,

    @SerializedName("outgoingsAmount")
    var outgoingsAmount : Double?,

    @SerializedName("branchId")
    var branchId : Int,

    @SerializedName("isSuperAdmin")
    var isSuperAdmin : Boolean,

    @SerializedName("paymentMethodList")
    var paymentMethodList : ArrayList<StadisticsData>?,

    @SerializedName("platformList")
    var platformList : ArrayList<StadisticsData>?,

    @SerializedName("montlySalesList")
    var montlySalesList : ArrayList<StadisticsData>?,

    @SerializedName("dishStatisticList")
    var dishStatisticList : ArrayList<StadisticsData>?,

    @SerializedName("waiterStatisticList")
    var waiterStatisticList : ArrayList<StadisticsData>?,

    @SerializedName("yearList")
    var yearList : ArrayList<Int>?
)

data class StadisticsData(

    @SerializedName("id")
    var id : String?,

    @SerializedName("name")
    var name : String?,

    @SerializedName("color")
    var color : String?,

    @SerializedName("totalAmount")
    var totalAmount : Double?,

    @SerializedName("monthNumber")
    var monthNumber : Int?,

    @SerializedName("timesOrdered")
    var timesOrdered : Int?
)