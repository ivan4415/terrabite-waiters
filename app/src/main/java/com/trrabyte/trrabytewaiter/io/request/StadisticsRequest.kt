package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

class StadisticsRequest {

    @SerializedName("branchId")
    var branchId : Int = -1

    @SerializedName("yearListFilter")
    var yearListFilter : ArrayList<Int> = arrayListOf()
}