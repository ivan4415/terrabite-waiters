package com.trrabyte.trrabytewaiter.io.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trrabyte.trrabytewaiter.io.response.Branch
import com.trrabyte.trrabytewaiter.io.response.User

@Database(entities = [Branch::class, User::class],version = 1)
abstract class WaitersDataBase : RoomDatabase(){
    abstract fun branchDao() : BranchDao
    abstract fun userDao() : UserDao
}