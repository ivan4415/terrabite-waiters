package com.trrabyte.trrabytewaiter.io.request

import com.google.gson.annotations.SerializedName

class QrRequest {

    @SerializedName("tableNum")
    var tableNum : Int = -1

    @SerializedName("branchId")
    var branchId : Int = -1
}