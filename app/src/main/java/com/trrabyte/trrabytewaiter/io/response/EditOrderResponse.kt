package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName

data class EditOrderResponse (

    @SerializedName("productCategoryList")
    var productCategoryList: ArrayList<ProductCategory>,

    @SerializedName("dishList")
    var dishList: ArrayList<Dish>,

    @SerializedName("occupiedTableList")
    var occupiedTableList : ArrayList<Int> = arrayListOf(),

    @SerializedName("paidList")
    var paidList : ArrayList<Dish> = arrayListOf(),

    @SerializedName("unpaidList")
    var unpaidList : ArrayList<Dish> = arrayListOf(),

    @SerializedName("orderTypeList")
    var orderType: ArrayList<OrderType> = arrayListOf(),

    @SerializedName("order")
    var order : Orders

)