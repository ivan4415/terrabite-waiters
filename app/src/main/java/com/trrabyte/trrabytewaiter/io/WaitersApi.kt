package com.trrabyte.trrabytewaiter.io

import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.io.request.*
import com.trrabyte.trrabytewaiter.io.response.*
import retrofit2.Call
import retrofit2.http.*

interface WaitersApi {

    @POST("signIn")
    fun doLogin(@Body loginRequest : LoginRequest) : Call<BaseResponse<LoginResponse>>

   @POST("orders/orders")
   fun getOrders(@Body ordersRequest : OrdersRequest) : Call<BaseResponse<OrdersResponse>>

   @POST("orders/new")
   fun newOrder(@Body ordersRequest: OrdersRequest) : Call<BaseResponse<NewOrderResponse>>

   @POST("orders/create")
   fun createOrder(@Body createOrdersRequest: CreateOrderRequest) : Call<BaseResponse<CreateOrderResponse>>

   @GET("orders/{id}/edit")
   fun getEditOrder(@Path("id")id : Int) : Call<BaseResponse<EditOrderResponse>>

  @POST("orders/update")
  fun updateOrder(@Body updateOrder : CreateOrderRequest) : Call<BaseResponse<EditOrderResponse>>

  @GET("orders/{id}/charge")
  fun getCharge(@Path("id") id : Int) : Call<BaseResponse<GetCheckResponse>>

  @POST("orders/cancel")
  fun cancelOrder(@Body order : OrdersRequest) : Call<BaseResponse<String>>

  @POST("orders/charge")
  fun chargeOrder(@Body chargeOrderRequest : ChargeOrderRequest) : Call<BaseResponse<String>>
//   region ResetPassword
   @POST("users/recovery")
   fun sendEmail(@Body email : RecoveryRequest) : Call<BaseResponse<String>>

   @POST("users/verifyCode")
   fun verifyCode(@Body code : VerifyCodeRequest) : Call<BaseResponse<UserRole>>

   @POST("users/resetPassword")
   fun resetPassword(@Body resetPassword : ResetPasswordRequest) : Call<BaseResponse<String>>
//   endregion


//   region Stadistics

   @POST("admin/statistics")
   fun getStadistics(@Body stadisticRequest : StadisticsRequest) : Call<BaseResponse<StadisticsResponse>>

//   endregion

//   region History

   @POST("orders/history")
   fun getHistory(@Body requestHistory : HistoryRequest) : Call<BaseResponse<OrdersResponse>>
//   endregion

//   region QR

   @GET("orders/qr")
   fun getQrOrder(@Query("tableNum")tableNum:Int,@Query("branchId")branchId : Int) : Call<BaseResponse<QrResponse>>


}