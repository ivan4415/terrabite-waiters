package com.trrabyte.trrabytewaiter.io.dao

import androidx.room.*
import com.trrabyte.trrabytewaiter.io.response.Branch

@Dao
interface BranchDao {

    @Query("SELECT * FROM branch")
    fun getAllBranch() : List<Branch>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll (branch : List<Branch>)

    @Query("DELETE FROM branch")
    fun deleteAll()
}
