package com.trrabyte.trrabytewaiter.io.response

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import java.io.Serializable

data class NewOrderResponse(

    @SerializedName("productCategoryList")
    var productCategoryList: ArrayList<ProductCategory>,

    @SerializedName("dishList")
    var dishList: ArrayList<Dish>,

    @SerializedName("folio")
    var folio : Int? = -1,

    @SerializedName("orderTypeList")
    var orderTypeList : ArrayList<OrderType>
)

data class ProductCategory(
    @SerializedName("productList")
    var productList: ArrayList<Product>
    ) : NewBaseResponse()

open class Product : NewBaseResponse(), Serializable {

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("price")
    var price: Double = -1.0

    @SerializedName("isPackage")
    var isPackage: Boolean? = false

    @SerializedName("imageUrl")
    var imageUrl: String? = ""

    @SerializedName("quantity")
    var count : Int = -1

//CreateOrderRequest
    @SerializedName("productId")
    var productId : Int? = null

   @SerializedName("productName")
    var productName : String? =""

    @SerializedName("subTotalAmount")
    var subTotalAmount : Double? = -1.1

//    EditOrder

    @SerializedName("orderID")
    var orderId : Int? = null

    @SerializedName("promotionId")
    var promotionId : Int? = null

    @SerializedName("orderDetailStatusId")
    var orderDetailStatusId : String? = ""

    @SerializedName("packageId")
    var packageId : Int? = null

    @SerializedName("status")
    var status : String? = ""

    fun toDish(categoryId : Int) : Dish{
        val dish = Dish()
        dish.description = this.description
        dish.price = this.price
        dish.isPackage = this.isPackage
        dish.imageUrl = this.imageUrl
        dish.count = this.count
        dish.productName = this.name
        dish.name = this.name
        dish.subTotalAmount = (this.price*this.count)
        dish.productId = this.productId ?: this.id
        dish.id = this.id ?: this.productId
        dish.productCategoriId  = categoryId
        return dish
    }
}
 class Dish : Product(){
    @SerializedName("productCategoryId")
    var productCategoriId: Int = -1

     fun clone() : Dish{
         val dish = Dish()
         dish.description = this.description
         dish.price = this.price
         dish.isPackage = this.isPackage
         dish.imageUrl = this.imageUrl
         dish.count = this.count
         dish.productName = if(TextUtils.isEmpty(this.productName)) this.name else this.productName
         dish.name = if(TextUtils.isEmpty(this.name))this.productName else this.name
         dish.subTotalAmount = subTotalAmount
         dish.productId = this.productId ?: this.id
         dish.id = this.id ?: this.productId
         dish.productCategoriId  = productCategoriId
         return dish
     }
}

data class OrderType(

    @SerializedName("id")
    var id : String? = "",

    @SerializedName("description")
    var description : String? = ""

)

