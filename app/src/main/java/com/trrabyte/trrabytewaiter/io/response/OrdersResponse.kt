package com.trrabyte.trrabytewaiter.io.response

import com.google.gson.annotations.SerializedName
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import java.io.Serializable
import java.nio.DoubleBuffer

data class OrdersResponse(

    @SerializedName("orders")
    var orders: ArrayList<Orders> = ArrayList(),

    @SerializedName("numTables")
    var numTables: Int = -1
)

data class Orders(
    @SerializedName("orderTypeId")
    var orderTypeId: String? = "",

    @SerializedName("orderStatusId")
    var orderStatusId: String? = "",

    @SerializedName("paymentMethodId")
    var paymentMethodId: String? = "",

    @SerializedName("folio")
    var folio: Int = -1,

    @SerializedName("tableNumber")
    var tableNumber: Int = -1,

    @SerializedName("totalAmount")
    var totalAmount: Double = -1.0,

    @SerializedName("chargeDate")
    var chargeDate: String? = "",

    @SerializedName("orderDate")
    var orderDate: String? = "",

    @SerializedName("orderDetailList")
    var orderDetailList: ArrayList<OrderDetail> = ArrayList(),

    @SerializedName("tipAmount")
    var tipAmount : Double? = 0.0

) : NewBaseResponse()

data class OrderDetail(
    @SerializedName("orderDetailStatusId")
    var orderDetailStatusID: String? = "",

    @SerializedName("promotionId")
    var promotionId: String? = "",

    @SerializedName("dishId")
    var dishId: Int? = -1,

    @SerializedName("dishName")
    var dishName: String? = "",

    @SerializedName("price")
    var price: Double? = 0.0,

    @SerializedName("quantity")
    var quantity: Int? = -1,

    @SerializedName("subTotalAmount")
    var subTotalAmount: Double? = 0.0
) : NewBaseResponse(){
    fun toDish() : Dish{
        val dish = Dish()
        dish.productCategoriId = -1
        dish.name = dishName
        dish.price = price ?: 0.0
        dish.count = quantity ?: 0
        dish.subTotalAmount =  dish.price * dish.count
        return dish
    }
}




