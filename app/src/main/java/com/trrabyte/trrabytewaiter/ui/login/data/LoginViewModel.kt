package com.trrabyte.trrabytewaiter.ui.login.data

import android.app.Application
import androidx.lifecycle.LiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.ui.BaseViewModel
import com.trrabyte.trrabytewaiter.io.request.LoginRequest
import com.trrabyte.trrabytewaiter.io.response.LoginResponse

class LoginViewModel (applcation : Application): BaseViewModel(applcation) {

    val repository by lazy { LoginRepository() }

    fun doLogin(user : String, pss : String) : LiveData<BaseResponse<LoginResponse>>{
        var request = LoginRequest()
        request.email = user
        request.password = pss
        return repository.doLogin(request)
    }
}