package com.trrabyte.trrabytewaiter.ui.utils.scanner.Codes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.trrabyte.trrabytewaiter.R;
import com.trrabyte.trrabytewaiter.base.io.ApplicationBase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by franciscobartilotti on 8/26/16.
 * This class houses the method to generate multiple 1D and 2D codes
 * You need to add to gradle: compile 'com.google.zxing:core:3.2.1'
 */
public class GenerateCodes {

    private static final int SIZELIMIT = 350;
    private static final int SIZEMIN = 200;

    public static boolean saveQr(Bitmap mBitmap, String path) {
        final File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
        if (mBitmap != null) {
            final FileOutputStream out;
            try {
                out = new FileOutputStream(file);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (final IOException e) {
//                LogUtils.e("GenerateCode", e.getMessage(), e.getCause());
            }
        }
        return file.exists();
    }


    public static Uri saveQr(Bitmap mBitmap) {
        Uri imageUri = null;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            String path = MediaStore.Images.Media.insertImage(BadApplication.getAppContext().getContentResolver(), mBitmap, "Red Pagamovíl Azteca", null);
            String path = MediaStore.Images.Media.insertImage(ApplicationBase.Companion.getInstance().ctx.getContentResolver(), mBitmap, "Red Pagamovíl Azteca", null);
            imageUri = Uri.parse(path);
        } catch (Exception e){
            imageUri = null;
        }
        return imageUri;
    }

    public static Bitmap buildQrBitmap(String data, int color, @Nullable Bitmap image,
                                       int imageSize, int codeSize) throws WriterException {
        Bitmap mBitmap;
        final Bitmap combined;
        final Bitmap overlay;
        final Writer writer = new QRCodeWriter();
        //final String finaldata = Uri.encode(data, "ISO-8859-1");

        final int w;
        final int h;

        if (codeSize == 0) {
            codeSize = 1;
        }
        w = codeSize;
        h = w;

        if (((float) imageSize / codeSize) >= 0.23) {
            imageSize = (int) ((float) codeSize * 0.22);
        }
        if (imageSize == 0) {
            imageSize = 1;
        }
        final Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        final BitMatrix bm = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
        final int[] pixels = new int[w * h];
        for (int y = 0; y < w; y++) {
            final int offset = y * h;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = bm.get(x, y) ? color : Color.WHITE;
            }
        }
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        combined = Bitmap.createBitmap(w, h, mBitmap.getConfig());

        final Canvas canvas = new Canvas(combined);
        final int canvasWidth = canvas.getWidth();
        final int canvasHeight = canvas.getHeight();
        canvas.drawBitmap(mBitmap, new Matrix(), null);

        if(image!=null) {
            overlay = Bitmap.createScaledBitmap(image, imageSize, imageSize, false);

            final int centreX = (canvasWidth - overlay.getWidth()) / 2;
            final int centreY = (canvasHeight - overlay.getHeight()) / 2;
            canvas.drawBitmap(overlay, centreX, centreY, null);
        }

        mBitmap = Bitmap.createBitmap(combined);

        return mBitmap;
    }

    public static QrItem doAllHere(Context context, String data, String codeType, int color, @Nullable Bitmap image, int imageSize, int codeSize, boolean resize, boolean saveFile, @Nullable String qrDir)
            throws IOException, WriterException, IllegalArgumentException, OutOfMemoryError {
        final QrItem qrItem = new QrItem();
        Bitmap mBitmap = null;
        File file = null;
        final Bitmap combined;
        Bitmap overlay;
        if ("QR_IMAGE".equals(codeType)) {
            if (saveFile) {
                file = new File(qrDir + File.separator + data + "_QR_IMAGE.png");
            }
            final Writer writer = new QRCodeWriter();
            //final String finaldata = Uri.encode(data, "ISO-8859-1");
            if (saveFile) {
                if (file.exists()) {
                    file.delete();
                }
            }
            final int w;
            final int h;
            if (resize && (codeSize > SIZELIMIT)) {
                w = SIZELIMIT;
                h = SIZELIMIT;
            } else {
                if (codeSize == 0) {
                    codeSize = 1;
                }
                w = codeSize;
                h = w;
            }
            if (resize) {
                if (codeSize < SIZELIMIT) {
                    if (((float) imageSize / codeSize) >= 0.21) {
                        imageSize = (int) ((float) codeSize * 0.20);
                    }
                } else {
                    if (((float) imageSize / SIZELIMIT) >= 0.23) {
                        imageSize = (int) ((float) SIZELIMIT * 0.22);
                    }
                }
            } else {
                if (((float) imageSize / codeSize) >= 0.23) {
                    imageSize = (int) ((float) codeSize * 0.22);
                }
            }
            if (imageSize == 0) {
                imageSize = 1;
            }
            final Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            final BitMatrix bm = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
            final int[] pixels = new int[w * h];
            for (int y = 0; y < w; y++) {
                final int offset = y * h;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = bm.get(x, y) ? color : Color.TRANSPARENT;
                }
            }
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);
            combined = Bitmap.createBitmap(w, h, mBitmap.getConfig());

            final Canvas canvas = new Canvas(combined);
            final int canvasWidth = canvas.getWidth();
            final int canvasHeight = canvas.getHeight();
            canvas.drawBitmap(mBitmap, new Matrix(), null);

            //if (image != null) {
            overlay = Bitmap.createBitmap(image);
            /*} else {
                overlay = BitmapFactory.decodeResource(context.getResources(), R.drawable.usersample);
            }*/

            overlay = Bitmap.createScaledBitmap(overlay, imageSize, imageSize, false);

            final int centreX = (canvasWidth - overlay.getWidth()) / 2;
            final int centreY = (canvasHeight - overlay.getHeight()) / 2;
            canvas.drawBitmap(overlay, centreX, centreY, null);

            if (resize) {
                if (codeSize > SIZELIMIT) {
                    final Matrix matrix = new Matrix();
                    final float scaleSize = (float) codeSize / SIZELIMIT;
                    matrix.postScale(scaleSize, scaleSize);
                    mBitmap = Bitmap.createBitmap(combined, 0, 0, SIZELIMIT, SIZELIMIT, matrix, false);
                } else if (codeSize >= SIZEMIN) {
                    mBitmap = Bitmap.createBitmap(combined);
                } else {
                    mBitmap = Bitmap.createScaledBitmap(combined, SIZEMIN, SIZEMIN, false);
                }
            } else {
                mBitmap = Bitmap.createBitmap(combined);
            }

            if (saveFile) {
                if (mBitmap != null) {
                    final FileOutputStream out = new FileOutputStream(file);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            } else {
                if (mBitmap != null) {
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            }
        } else {
            final BarcodeFormat codeFormat;
            switch (codeType) {
                case "AZTEC":
                    codeFormat = BarcodeFormat.AZTEC;
                    break;
                case "CODABAR":
                    codeFormat = BarcodeFormat.CODABAR;
                    break;
                case "CODE_39":
                    codeFormat = BarcodeFormat.CODE_39;
                    break;
                case "CODE_93":
                    codeFormat = BarcodeFormat.CODE_93;
                    break;
                case "CODE_128":
                    codeFormat = BarcodeFormat.CODE_128;
                    break;
                case "DATA_MATRIX":
                    codeFormat = BarcodeFormat.DATA_MATRIX;
                    break;
                case "EAN_8":
                    codeFormat = BarcodeFormat.EAN_8;
                    break;
                case "EAN_13":
                    codeFormat = BarcodeFormat.EAN_13;
                    break;
                case "ITF":
                    codeFormat = BarcodeFormat.ITF;
                    break;
                case "MAXICODE":
                    codeFormat = BarcodeFormat.MAXICODE;
                    break;
                case "PDF_417":
                    codeFormat = BarcodeFormat.PDF_417;
                    break;
                case "QR_CODE":
                    codeFormat = BarcodeFormat.QR_CODE;
                    break;
                case "RSS_14":
                    codeFormat = BarcodeFormat.RSS_14;
                    break;
                case "RSS_EXPANDED":
                    codeFormat = BarcodeFormat.RSS_EXPANDED;
                    break;
                case "UPC_A":
                    codeFormat = BarcodeFormat.UPC_A;
                    break;
                case "UPC_E":
                    codeFormat = BarcodeFormat.UPC_E;
                    break;
                case "UPC_EAN_EXTENSION":
                    codeFormat = BarcodeFormat.UPC_EAN_EXTENSION;
                    break;
                default:
                    codeFormat = BarcodeFormat.QR_CODE;
                    codeType = "QR_CODE";
                    break;
            }
            if (saveFile) {
                // File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                file = new File(Environment.getExternalStorageDirectory() + File.separator + "baz_bar_code.png");
                if (file.exists()) {
                    file.delete();
                }
            }
            final MultiFormatWriter writer = new MultiFormatWriter();
            BitMatrix result = null;
            Map<EncodeHintType, Object> hints2 = null;
            for (int i = 0; i < data.length(); i++) {
                if (data.charAt(i) > 0xFF) {
                    hints2 = new EnumMap<>(EncodeHintType.class);
                    hints2.put(EncodeHintType.CHARACTER_SET, "UTF-8");
                    break;
                }
            }

            final int w;
            final int h;
            if (resize && (codeSize > SIZELIMIT)) {
                w = SIZELIMIT;
                h = SIZELIMIT;
            } else {
                w = codeSize;
                h = w;
            }

            result = writer.encode(data, codeFormat, w, h, hints2);
            final int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                final int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? color : Color.WHITE;
                }
            }

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);

            if (resize) {
                if (codeSize > SIZELIMIT) {
                    final Matrix matrix = new Matrix();
                    final float scaleSize = (float) codeSize / SIZELIMIT;
                    matrix.postScale(scaleSize, scaleSize);
                    mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, SIZELIMIT, SIZELIMIT, matrix, false);
                } else if (codeSize < SIZEMIN) {
                    mBitmap = Bitmap.createScaledBitmap(mBitmap, SIZEMIN, SIZEMIN, false);
                }
            }

            if (saveFile) {
                if (mBitmap != null) {
                    final FileOutputStream out = new FileOutputStream(file);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            } else {
                if (mBitmap != null) {
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            }
        }
        return qrItem;
    }

    public static QrItem forBranch(Context context, String data, String codeType, int color, @Nullable Bitmap image, int imageSize, int codeSize, boolean resize, boolean saveFile, @Nullable String qrDir) throws IOException, WriterException, IllegalArgumentException, OutOfMemoryError {
        final QrItem qrItem = new QrItem();
        Bitmap mBitmap = null;
        File file = null;
        final Bitmap combined;
        Bitmap overlay;
        if ("QR_IMAGE".equals(codeType)) {
            if (saveFile) {
                file = new File(qrDir + File.separator + data + "_QR_IMAGE.png");
            }
            final Writer writer = new QRCodeWriter();
            final String finaldata = Uri.encode(data, "ISO-8859-1");
            if (saveFile) {
                if (file.exists()) {
                    file.delete();
                }
            }
            final int w;
            final int h;
            if (resize && (codeSize > SIZELIMIT)) {
                w = SIZELIMIT;
                h = SIZELIMIT;
            } else {
                if (codeSize == 0) {
                    codeSize = 1;
                }
                w = codeSize;
                h = w;
            }
            if (resize) {
                if (codeSize < SIZELIMIT) {
                    if (((float) imageSize / codeSize) >= 0.21) {
                        imageSize = (int) ((float) codeSize * 0.20);
                    }
                } else {
                    if (((float) imageSize / SIZELIMIT) >= 0.23) {
                        imageSize = (int) ((float) SIZELIMIT * 0.22);
                    }
                }
            } else {
                if (((float) imageSize / codeSize) >= 0.23) {
                    imageSize = (int) ((float) codeSize * 0.22);
                }
            }
            if (imageSize == 0) {
                imageSize = 1;
            }
            final Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            final BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE, w, h, hints);
            final int[] pixels = new int[w * h];
            for (int y = 0; y < w; y++) {
                final int offset = y * h;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = bm.get(x, y) ? color : Color.TRANSPARENT;
                }
            }
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);
            combined = Bitmap.createBitmap(w, h, mBitmap.getConfig());

            final Canvas canvas = new Canvas(combined);
            final int canvasWidth = canvas.getWidth();
            final int canvasHeight = canvas.getHeight();
            canvas.drawBitmap(mBitmap, new Matrix(), null);

            if (image != null) {
                overlay = Bitmap.createBitmap(image);
            } else {
                overlay = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_add);

            }

            overlay = Bitmap.createScaledBitmap(overlay, imageSize, imageSize, false);

            final int centreX = (canvasWidth - overlay.getWidth()) / 2;
            final int centreY = (canvasHeight - overlay.getHeight()) / 2;
            canvas.drawBitmap(overlay, centreX, centreY, null);

            if (resize) {
                if (codeSize > SIZELIMIT) {
                    final Matrix matrix = new Matrix();
                    final float scaleSize = (float) codeSize / SIZELIMIT;
                    matrix.postScale(scaleSize, scaleSize);
                    mBitmap = Bitmap.createBitmap(combined, 0, 0, SIZELIMIT, SIZELIMIT, matrix, false);
                } else if (codeSize >= SIZEMIN) {
                    mBitmap = Bitmap.createBitmap(combined);
                } else {
                    mBitmap = Bitmap.createScaledBitmap(combined, SIZEMIN, SIZEMIN, false);
                }
            } else {
                mBitmap = Bitmap.createBitmap(combined);
            }

            if (saveFile) {
                if (mBitmap != null) {
                    final FileOutputStream out = new FileOutputStream(file);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            } else {
                if (mBitmap != null) {
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            }
        } else {
            int w = 0;
            int h = 0;
            final BarcodeFormat codeFormat;
            switch (codeType) {
                case "AZTEC":
                    codeFormat = BarcodeFormat.AZTEC;
                    break;
                case "CODABAR":
                    w = 1200;
                    h = 260;
                    codeFormat = BarcodeFormat.CODABAR;
                    break;
                case "CODE_39":
                    codeFormat = BarcodeFormat.CODE_39;
                    break;
                case "CODE_93":
                    codeFormat = BarcodeFormat.CODE_93;
                    break;
                case "CODE_128":
                    w = 1200;
                    h = 260;
                    codeFormat = BarcodeFormat.CODE_128;
                    break;
                case "DATA_MATRIX":
                    codeFormat = BarcodeFormat.DATA_MATRIX;
                    break;
                case "EAN_8":
                    codeFormat = BarcodeFormat.EAN_8;
                    break;
                case "EAN_13":
                    codeFormat = BarcodeFormat.EAN_13;
                    break;
                case "ITF":
                    codeFormat = BarcodeFormat.ITF;
                    break;
                case "MAXICODE":
                    codeFormat = BarcodeFormat.MAXICODE;
                    break;
                case "PDF_417":
                    codeFormat = BarcodeFormat.PDF_417;
                    break;
                case "QR_CODE":
                    w = 400;
                    h = 400;
                    codeFormat = BarcodeFormat.QR_CODE;
                    break;
                case "RSS_14":
                    codeFormat = BarcodeFormat.RSS_14;
                    break;
                case "RSS_EXPANDED":
                    codeFormat = BarcodeFormat.RSS_EXPANDED;
                    break;
                case "UPC_A":
                    codeFormat = BarcodeFormat.UPC_A;
                    break;
                case "UPC_E":
                    codeFormat = BarcodeFormat.UPC_E;
                    break;
                case "UPC_EAN_EXTENSION":
                    codeFormat = BarcodeFormat.UPC_EAN_EXTENSION;
                    break;
                default:
                    codeFormat = BarcodeFormat.QR_CODE;
                    codeType = "QR_CODE";
                    break;
            }
            if (saveFile) {
                // File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                file = new File(Environment.getExternalStorageDirectory() + File.separator + "baz_bar_code.png");
                if (file.exists()) {
                    file.delete();
                }
            }
            final MultiFormatWriter writer = new MultiFormatWriter();
            BitMatrix result = null;
            Map<EncodeHintType, Object> hints2 = null;
            for (int i = 0; i < data.length(); i++) {
                if (data.charAt(i) > 0xFF) {
                    hints2 = new EnumMap<>(EncodeHintType.class);
                    hints2.put(EncodeHintType.CHARACTER_SET, "UTF-8");
                    break;
                }
            }


//            if (resize && codeSize > SIZELIMIT) {
//                w = SIZELIMIT;
//                h = SIZELIMIT;
//            } else {
//                w = 1200;
//                h = 260;
//            }

            result = writer.encode(data, codeFormat, w, h, hints2);
            final int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                final int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
                }
            }

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);

            if (resize) {
                if (codeSize > SIZELIMIT) {
                    final Matrix matrix = new Matrix();
                    final float scaleSize = (float) codeSize / SIZELIMIT;
                    matrix.postScale(scaleSize, scaleSize);
                    mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, SIZELIMIT, SIZELIMIT, matrix, false);
                } else if (codeSize < SIZEMIN) {
                    mBitmap = Bitmap.createScaledBitmap(mBitmap, SIZEMIN, SIZEMIN, false);
                }
            }

            if (saveFile) {
                if (mBitmap != null) {
                    final FileOutputStream out = new FileOutputStream(file);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            } else {
                if (mBitmap != null) {
                    qrItem.bitmap = Bitmap.createBitmap(mBitmap);
                }
            }
        }
        return qrItem;
    }

    public static QrItem doAllHere(String data, String codeType, int color, int height, int codeSize, boolean resize, boolean saveFile, @Nullable String qrDir) throws IOException, WriterException, IllegalArgumentException, OutOfMemoryError {
        //Tamaños que sirven:
        //CODABAR y CODE_128 16-24 caracteres, 400x800, 200x800
        //probado en un layout con 360dpx480dp en un moto g3
        final QrItem qrItem = new QrItem();
        Bitmap mBitmap;
        File file = null;

        final BarcodeFormat codeFormat;
        switch (codeType) {
            case "AZTEC":
                codeFormat = BarcodeFormat.AZTEC;
                break;
            case "CODABAR":
                codeFormat = BarcodeFormat.CODABAR;
                break;
            case "CODE_39":
                codeFormat = BarcodeFormat.CODE_39;
                break;
            case "CODE_93":
                codeFormat = BarcodeFormat.CODE_93;
                break;
            case "CODE_128":
                codeFormat = BarcodeFormat.CODE_128;
                break;
            case "DATA_MATRIX":
                codeFormat = BarcodeFormat.DATA_MATRIX;
                break;
            case "EAN_8":
                codeFormat = BarcodeFormat.EAN_8;
                break;
            case "EAN_13":
                codeFormat = BarcodeFormat.EAN_13;
                break;
            case "ITF":
                codeFormat = BarcodeFormat.ITF;
                break;
            case "MAXICODE":
                codeFormat = BarcodeFormat.MAXICODE;
                break;
            case "PDF_417":
                codeFormat = BarcodeFormat.PDF_417;
                break;
            case "QR_CODE":
                codeFormat = BarcodeFormat.QR_CODE;
                break;
            case "RSS_14":
                codeFormat = BarcodeFormat.RSS_14;
                break;
            case "RSS_EXPANDED":
                codeFormat = BarcodeFormat.RSS_EXPANDED;
                break;
            case "UPC_A":
                codeFormat = BarcodeFormat.UPC_A;
                break;
            case "UPC_E":
                codeFormat = BarcodeFormat.UPC_E;
                break;
            case "UPC_EAN_EXTENSION":
                codeFormat = BarcodeFormat.UPC_EAN_EXTENSION;
                break;
            default:
                codeFormat = BarcodeFormat.QR_CODE;
                codeType = "QR_CODE";
                break;
        }
        if (saveFile) {
            file = new File(Environment.getExternalStorageDirectory() + File.separator + "baz_bar_code.png");

            //  file = new File(qrDir + File.separator + data + "_" + codeType + ".jpg");
            if (file.exists()) {
                file.delete();
            }
        }
        final MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = null;
        Map<EncodeHintType, Object> hints2 = null;
        for (int i = 0; i < data.length(); i++) {
            if (data.charAt(i) > 0xFF) {
                hints2 = new EnumMap<>(EncodeHintType.class);
                hints2.put(EncodeHintType.CHARACTER_SET, "UTF-8");
                break;
            }
        }

        final int w;
        final int h;
        if (resize && (codeSize > SIZELIMIT)) {
            w = SIZELIMIT;
            h = SIZELIMIT / 2;
        } else {
            w = codeSize;
            h = height;
        }

        result = writer.encode(data, codeFormat, w, h, hints2);
        final int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            final int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? color : Color.TRANSPARENT;
            }
        }

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mBitmap.setPixels(pixels, 0, w, 0, 0, w, h);

        if (resize) {
            if (codeSize > SIZELIMIT) {
                final Matrix matrix = new Matrix();
                final float scaleSize = (float) codeSize / SIZELIMIT;
                matrix.postScale(scaleSize, scaleSize);
                mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, SIZELIMIT, SIZELIMIT, matrix, false);
            } else if (codeSize < SIZEMIN) {
                mBitmap = Bitmap.createScaledBitmap(mBitmap, SIZEMIN, SIZEMIN, false);
            }
        }

        if (saveFile) {
            if (mBitmap != null) {
                final FileOutputStream out = new FileOutputStream(file);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
                qrItem.bitmap = Bitmap.createBitmap(mBitmap);
            }
        } else {
            if (mBitmap != null) {
                qrItem.bitmap = Bitmap.createBitmap(mBitmap);
            }
        }

        return qrItem;
    }

}
