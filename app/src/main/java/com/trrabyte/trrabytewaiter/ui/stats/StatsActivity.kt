package com.trrabyte.trrabytewaiter.ui.stats

import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.BaseActivity
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentHistory
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentMain
import com.trrabyte.trrabytewaiter.ui.stats.data.StatsViewModel
import com.trrabyte.trrabytewaiter.ui.stats.fragments.FragmentStats
import kotlinx.android.synthetic.main.fragment_main.*

class StatsActivity : BaseActivity<StatsViewModel>(R.navigation.navigation_stats){

    override fun onBackPressed() {
        val f : Fragment = (supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment).childFragmentManager.fragments[0] ?: Fragment()
        if (f is FragmentStats) {
            if(drawer_layout.isOpen){
                drawer_layout.closeDrawer(GravityCompat.START)
            }else{
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}