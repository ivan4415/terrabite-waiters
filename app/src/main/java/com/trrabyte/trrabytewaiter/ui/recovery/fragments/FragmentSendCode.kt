package com.trrabyte.trrabytewaiter.ui.recovery.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.ui.recovery.data.RecoveryViewModel
import kotlinx.android.synthetic.main.fragment_recovery_email.*

class FragmentSendCode : BaseFragment<RecoveryViewModel>(RecoveryViewModel::class.java) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_recovery_email,container,false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ti_email.hint = "Codigo"
    }

    override fun setListeners() {
        super.setListeners()
        btn_continue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.btn_continue->{
                showProgress()
                fragmentViewModel?.sendCode(et_email.text.toString())?.observe(viewLifecycleOwner,
                    Observer {
                        if(it.hasError){
                            Utils.showSimpleDialog(requireContext(),message = it.message)
                        }else{
                            val b = Bundle()
                            b.putInt("userId",it.data?.userId?:-1)
                            navigateToFragment(R.id.to_change_pass,b)
                        }
                        hideProgress()
                    })
            }
        }
    }
}