package com.trrabyte.trrabytewaiter.ui.main

import android.view.Gravity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.BaseActivity
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentHistory
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentMain
import com.trrabyte.trrabytewaiter.ui.utils.scanner.BarCodeFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainActivity : BaseActivity<MainViewModel>(R.navigation.navigation_main) {

    override fun onBackPressed() {
        val f : Fragment = (supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment).childFragmentManager.fragments[0] ?: Fragment()
        if (f is FragmentMain||f is FragmentHistory || f is BarCodeFragment) {
            if(drawer_layout.isOpen){
              drawer_layout.closeDrawer(GravityCompat.START)
            }else{
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}