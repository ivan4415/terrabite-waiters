package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.Orders
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.utils.PickerDialogFragment
import com.trrabyte.trrabytewaiter.ui.utils.adapters.OrdersAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.jar.Pack200

class FragmentMain : BaseFragment<MainViewModel>(MainViewModel::class.java) {

    var numOfTables = -1
    var tables = ArrayList<NewBaseResponse>()
    var PERMISSION_RESULT = 4415


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_main, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab_add.setOnClickListener(this)
        fragmentViewModel?.setCar(HashMap())
        ic_menu.visibility = View.VISIBLE
        tv_branch.visibility = View.VISIBLE
        ic_menu.setOnClickListener(this)
        tv_branch.setOnClickListener(this)
    }

    override fun setObservers() {
        super.setObservers()
        setBranchPicker()
        setDrawerView()
        setFragmentObserver()
    }

    private val  requestPermissions = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if(isGranted){
            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_QR)
        }else{
            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
        }
    }
    private fun setFragmentObserver(){
        var json = "{\"tableNum\":4,\"branchId\":2}"
        fragmentViewModel?.getFragment()?.observe(viewLifecycleOwner, Observer {
            when(it?: MainViewModel.FragmentsMenu.DEFAULT){
                MainViewModel.FragmentsMenu.FRAGMENT_ORDERS -> drawer_layout.closeDrawer(GravityCompat.START)
                MainViewModel.FragmentsMenu.FRAGMENT_QR -> {
                    when {
                        ContextCompat.checkSelfPermission(requireContext(),Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED -> {
                            drawer_layout.closeDrawer(GravityCompat.START)
                            navigateToFragment(R.id.fragmentMain_to_barCodeFragment)
                            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                        }
//                        shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> {
//                            Utils.showSimpleDialog(requireContext(),message = getString(R.string.camara_permission)
//                                ,positiveTextButton = getString(R.string.aceptar),positiveListener ={_,_->
//                                    val i = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//                                    i.data = Uri.fromParts("package","com.trrabyte.trrabytewaiter",null)
//                                    startActivity(i)
//                                },negativeText = getString(R.string.cancelar),
//                                negativeListener = DialogInterface.OnClickListener{d,_->
//                                    d.dismiss()
//                                }
//                            )
//                        }
                        else -> {
                            requestPermissions.launch(Manifest.permission.CAMERA)
//                            requestPermissions( arrayOf(Manifest.permission.CAMERA),PERMISSION_RESULT)
                        }
                    }
                }
                MainViewModel.FragmentsMenu.FRAGMENT_HISTORY -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                    navigateToFragment(R.id.to_fragment_history)
                    fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                }
                MainViewModel.FragmentsMenu.DEFAULT -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
            }
        })
    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when(requestCode){
//            PERMISSION_RESULT->{
//                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//                    fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_QR)
//                }else{
//                    Utils.showSimpleDialog(requireContext(),message = getString(R.string.CAMARA_PERMISSION_DENY))
//                }
//                return
//            }
//        }
//
//    }

    private fun setBranchPicker() {
        if (fragmentViewModel?.getBranchId() == -1) {
            PickerDialogFragment(fragmentViewModel?.getBranch() ?: arrayListOf())
                .setOnBranchSelected {
                    fragmentViewModel?.setBranchId(it.id ?: -1)
                    tv_branch.text = it.name!!.split("-")[1]
                    getOrders()
                }
                .show(parentFragmentManager, "SHOW")
        } else {
            getOrders()
        }
    }

    private fun getOrders() {
        showProgress()
        fragmentViewModel?.getOrders()?.observe(viewLifecycleOwner, Observer {
            if (it.hasError) {
                Utils.showSimpleDialog(requireContext(), message = it.message)
            } else {
                it.data?.run {
                    numOfTables = numTables
                    setRecycler(orders)
                }
            }
            hideProgress()
        })
    }

    private fun setRecycler(lst: ArrayList<Orders>) {
        val adapter = OrdersAdapter(requireContext(), lst)
            .setBtnClickListener { orders ->
                orders.id?.let { id ->
                    val b = Bundle()
                    b.putInt("id", id)
                    b.putSerializable("dishList",orders.orderDetailList)
                    navigateToFragment(R.id.to_fragment_check, b)
                }
            }
            .setBtnCancelClickListener { order ->
                order.id?.let { id ->
                    Utils.showSimpleDialog(
                        requireContext(),
                        message = "¿Seguro que quieres cancelar la orden?",
                        positiveTextButton = "Si",
                        positiveListener =  { d, _ ->
                            fragmentViewModel?.cancelOrder(id)
                                ?.observe(viewLifecycleOwner,  {
                                    if (it.hasError) {
                                        d.dismiss()
                                        Utils.showSimpleDialog(
                                            requireContext(),
                                            message = it.message
                                        )
                                    } else {
                                        getOrders()
                                        d.dismiss()
                                    }
                                })
                        },
                        negativeText = "No",
                        negativeListener = { d, _ ->
                            d.dismiss()
                        }
                    )

                }
            }
            .setOnItemClicListener {
                var b = Bundle()
                b.putInt("id", it.id ?: -1)
                b.putSerializable("tables", tables)
                navigateToFragment(R.id.to_fragmentadd, b)
            }

        rv_orders.adapter = adapter
        rv_orders.layoutManager = LinearLayoutManager(requireContext())
        setTables()
    }

    private fun setTables() {
        for (i in 0..numOfTables) {
            val table = NewBaseResponse()
            table.id = i
            table.name = "M-${i + 1}"
            tables.add(table)
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.fab_add -> {
                val b = Bundle()
                b.putInt("numOfTables", numOfTables)
                b.putSerializable("tables", tables)
                navigateToFragment(R.id.to_fragmentadd, b)
            }
            R.id.tv_branch -> {
                PickerDialogFragment(fragmentViewModel?.getBranch() ?: arrayListOf())
                    .setOnBranchSelected {
                        fragmentViewModel?.setBranchId(it.id ?: -1)
                        tv_branch.text = it.name!!.split("-")[1]
                        getOrders()
                    }
                    .show(parentFragmentManager, "SHOW")
            }
            R.id.ic_menu->{
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun setDrawerView() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.ll_drawer_start, FragmentMenu(), "")
            .disallowAddToBackStack()
            .commitAllowingStateLoss()
    }
}