package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import com.trrabyte.trrabytewaiter.base.ui.utils.BaseAdapter
import kotlinx.android.synthetic.main.item_tables.view.*

class TablesAdapter(var ctx : Context,var lst : ArrayList<NewBaseResponse>) : PagerAdapter() {


//    override fun onBindViewHolder(holder: TablesViewHolder, position: Int) {
//        holder.tv_table_name.text = lst[position].name
//        holder.tv_table_name.setOnClickListener{
//            itemClickListener?.invoke(lst[position])
//        }
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TablesViewHolder {
//        return TablesViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_tables,parent,false))
//    }
//
//    class TablesViewHolder(itemView : View) : BaseAdapter.BaseViewHolder(itemView){
//        val tv_table_name : TextView = itemView.findViewById(R.id.tv_table_name)
//    }

    var positionReal = -1

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
//        super.instantiateItem(container, position)
        when {
            positionReal == -1 -> {
                positionReal = position
            }
            positionReal < position -> {
                positionReal = position - 1
            }
            positionReal > position -> {
                positionReal = position + 1
            }
        }
        val layoutInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val itemView = layoutInflater.inflate(R.layout.item_tables, container, false)
        itemView.tv_table_name.text = "M ${position+1}"

        container.addView(itemView)
        return itemView
    }
    override fun isViewFromObject(container: View, position: Any): Boolean {
        return container == position
    }
    override fun getCount(): Int {
        return lst.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }

}

