package com.trrabyte.trrabytewaiter.ui.utils.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment : DialogFragment() {

    companion object{
        fun newInstance(listener : DatePickerDialog.OnDateSetListener) : DatePickerFragment{
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }
    private var listener : DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val dialog = DatePickerDialog(requireActivity(),listener,c[Calendar.YEAR],c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
        dialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
        return dialog
    }


}