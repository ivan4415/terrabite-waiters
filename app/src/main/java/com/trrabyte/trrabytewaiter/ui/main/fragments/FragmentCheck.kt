package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.Dish
import com.trrabyte.trrabytewaiter.io.response.OrderDetail
import com.trrabyte.trrabytewaiter.io.response.PaymentMethodsResponse
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import kotlinx.android.synthetic.main.fragment_check.*
import okhttp3.internal.Util

class FragmentCheck : BaseFragment<MainViewModel>(MainViewModel::class.java) {

    private var orderID : Int = -1
    private var table = -1
    private var folio = -1
    private val dishList = arrayListOf<Dish>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_check,container,false)
        return v
    }

    override fun setListeners() {
        super.setListeners()
        btn_pagar.setOnClickListener(this)
    }

    override fun setObservers() {
        super.setObservers()
        arguments?.getInt("id")?.let {id->
            orderID = id
            showProgress()
            fragmentViewModel?.getPaymentMethod(id)?.observe(viewLifecycleOwner, Observer {
                if(it.hasError){
                    Utils.showSimpleDialog(requireContext(),message = it.message)
                }else{
                    it.data?.run {
                        setCheck(
                            paymentMethods ?: arrayListOf<PaymentMethodsResponse>(),
                            order.totalAmount ?: 0.0,
                            order.tableNumber?:0,order.folio?:0
                        )
                    }
                }
                hideProgress()
            })
        }

        val detailOrder = requireArguments().getSerializable("dishList") as ArrayList<OrderDetail>
        detailOrder.forEach {
            dishList.add(it.toDish())
        }
    }

    private fun setCheck(paymentMethodsResponse: ArrayList<PaymentMethodsResponse>,totalAmount : Double,tableNumber : Int,folio : Int){
        paymentMethodsResponse.forEach{
            when(it.id){
                "card"-> rb_card.visibility = View.VISIBLE
                "cash"->rb_cash.visibility = View.VISIBLE
            }
        }

        this.table = tableNumber
        this.folio = folio
        tv_table_number.text = "Mesa $tableNumber"
        tv_cash_amount.text  = "$ ${String.format("%.2f",totalAmount)}"
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.btn_pagar->{
                var payMethod = getPaymentId()
                showProgress()
                fragmentViewModel?.chargeOrder(orderID,payMethod)?.observe(viewLifecycleOwner,
                    Observer {
                        if(it.hasError){
                            Utils.showSimpleDialog(requireContext(),message = it.message)
                        }else{
                            Utils.printBt(requireContext(),
                                table = table,folio = folio,products = dishList)
                            Utils.showSimpleDialog(requireContext(),message = "Cuenta cerrada",positiveListener = DialogInterface.OnClickListener{d,_->
                                requireActivity().onBackPressed()
                            })
                        }
                        hideProgress()
                    })
            }
        }
    }

    private fun getPaymentId() : String{
        val id = rg_paymethods.checkedRadioButtonId
        return if(id == R.id.rb_card)  "card" else if(id == R.id.rb_cash) "cash" else "both"
    }
}
