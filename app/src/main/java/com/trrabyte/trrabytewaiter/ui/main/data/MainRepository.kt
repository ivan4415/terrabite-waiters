package com.trrabyte.trrabytewaiter.ui.main.data

import android.preference.PreferenceManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.SUCCESS
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.setResponseError
import com.trrabyte.trrabytewaiter.base.ui.BaseRepository
import com.trrabyte.trrabytewaiter.io.BadApplication
import com.trrabyte.trrabytewaiter.io.request.*
import com.trrabyte.trrabytewaiter.io.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository : BaseRepository() {

    var folio = -1
    fun getMenu(req: OrdersRequest) : LiveData<BaseResponse<NewOrderResponse>>{
        val res = MutableLiveData<BaseResponse<NewOrderResponse>>()
        BadApplication.getInstance().controllerWaiters.newOrder(req).enqueue(object :
            Callback<BaseResponse<NewOrderResponse>> {
            override fun onResponse(
                call: Call<BaseResponse<NewOrderResponse>>,
                response: Response<BaseResponse<NewOrderResponse>>
            ) {
                response.body()?.run{
                    when(codigoOperacion){
                        SUCCESS-> {
                            res.value = this
                            folio = data?.folio?:-1
                        }

                        else->res.value = setResponseError(message)
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse<NewOrderResponse>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }

        })
        return res
    }

    fun chargeOrder(req : ChargeOrderRequest) : LiveData<BaseResponse<String>>{
        var res = MutableLiveData<BaseResponse<String>>()
        BadApplication.getInstance().controllerWaiters.chargeOrder(req).service(res)
        return res
    }

    fun createOrder(req : CreateOrderRequest) : LiveData<BaseResponse<CreateOrderResponse>>{
        req.folio = folio
        val res = MutableLiveData<BaseResponse<CreateOrderResponse>>()
        BadApplication.getInstance().controllerWaiters.createOrder(req).service(res)
        return res
    }

    fun getOrders(req : OrdersRequest) : LiveData<BaseResponse<OrdersResponse>>{
        val res = MutableLiveData<BaseResponse<OrdersResponse>>()
        BadApplication.getInstance().controllerWaiters.getOrders(req).service(res)
        return res
    }

    fun getEditOrder(id : Int) : LiveData<BaseResponse<EditOrderResponse>>{
        val res = MutableLiveData<BaseResponse<EditOrderResponse>>()
        BadApplication.getInstance().controllerWaiters.getEditOrder(id).enqueue(object : Callback<BaseResponse<EditOrderResponse>>{
            override fun onResponse(
                call: Call<BaseResponse<EditOrderResponse>>,
                response: Response<BaseResponse<EditOrderResponse>>
            ) {
                response.body()?.run {
                    when(codigoOperacion){
                        SUCCESS->{
                            folio = data?.order?.folio?:-1
                            res.value = this
                        }
                        else->{
                            res.value = setResponseError(message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse<EditOrderResponse>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }

        })
        return res
    }

    fun cancelOrder(req : OrdersRequest) : LiveData<BaseResponse<String>>{
        val res = MutableLiveData<BaseResponse<String>>()
        BadApplication.getInstance().controllerWaiters.cancelOrder(req).service(res)
        return res
    }

    fun getBranch() : ArrayList<Branch>{
//        val res = MutableLiveData<ArrayList<Branch>>()
        return BadApplication.getInstance().db.branchDao().getAllBranch() as ArrayList<Branch>
//        return res
    }

    fun getPaymentMethod(req : Int) : LiveData<BaseResponse<GetCheckResponse>>{
        val res = MutableLiveData<BaseResponse<GetCheckResponse>>()
        BadApplication.getInstance().controllerWaiters.getCharge(req).service(res)
        return res
    }

    fun updateOrder(req : CreateOrderRequest) : LiveData<BaseResponse<EditOrderResponse>>{
        val res = MutableLiveData<BaseResponse<EditOrderResponse>>()
        req.folio = folio
        BadApplication.getInstance().controllerWaiters.updateOrder(req).service(res)
        return res
    }

    fun closeSession(){
        val sharedPrferences = PreferenceManager.getDefaultSharedPreferences(BadApplication.getInstance().applicationContext)
        val editor = sharedPrferences.edit()
        editor.clear()
        editor.apply()
        BadApplication.getInstance().db.userDao().deleteAll()
        BadApplication.getInstance().db.branchDao().deleteAll()

    }

    fun getUser() : User{
        return BadApplication.getInstance().db.userDao().getUser()
    }

    fun getHistory(req : HistoryRequest) : LiveData<BaseResponse<OrdersResponse>>{
        val re = MutableLiveData<BaseResponse<OrdersResponse>>()
        BadApplication.getInstance().controllerWaiters.getHistory(req).service(re)
        return re
    }

    fun getQrOrder(req : QrRequest) : LiveData<BaseResponse<QrResponse>>{
        val re = MutableLiveData<BaseResponse<QrResponse>>()
        BadApplication.getInstance().controllerWaiters.getQrOrder(req.tableNum,req.branchId).service(re)
        return re
    }
}