package com.trrabyte.trrabytewaiter.ui.recovery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.BaseActivity
import com.trrabyte.trrabytewaiter.ui.recovery.data.RecoveryViewModel

class RecoveryActivity : BaseActivity<RecoveryViewModel>(R.navigation.navigation_recovery)