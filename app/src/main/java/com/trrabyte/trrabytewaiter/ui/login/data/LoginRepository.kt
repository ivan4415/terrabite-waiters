package com.trrabyte.trrabytewaiter.ui.login.data

import android.preference.PreferenceManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.SUCCESS
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.setResponseError
import com.trrabyte.trrabytewaiter.base.ui.BaseRepository
import com.trrabyte.trrabytewaiter.io.BadApplication
import com.trrabyte.trrabytewaiter.io.request.LoginRequest
import com.trrabyte.trrabytewaiter.io.response.Branch
import com.trrabyte.trrabytewaiter.io.response.LoginResponse
import com.trrabyte.trrabytewaiter.io.response.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


open class LoginRepository : BaseRepository() {


    fun doLogin(request : LoginRequest) : LiveData<BaseResponse<LoginResponse>>{
        val res = MutableLiveData<BaseResponse<LoginResponse>>()
        BadApplication.getInstance().controllerWaiters.doLogin(request).enqueue(object : Callback<BaseResponse<LoginResponse>>{
            override fun onResponse(call: Call<BaseResponse<LoginResponse>>, response: Response<BaseResponse<LoginResponse>>) {
                response.body()?.run{
                    when(codigoOperacion){
                        SUCCESS ->{
                            val sharedPrferences = PreferenceManager.getDefaultSharedPreferences(BadApplication.getInstance().applicationContext)
                            val editor = sharedPrferences.edit()
                            editor.putString("token",data?.token)
                            editor.apply()
                            saveBranch(data?.branchList)
                            saveUser(data?.credential?.user)
                            res.value = this
                        }
                        else->{
                            res.value = setResponseError(message)
                        }
                    }

                }
            }

            override fun onFailure(call: Call<BaseResponse<LoginResponse>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }

        })
        return res
    }

    fun saveBranch(branchList: ArrayList<Branch>?){
        branchList?.let {
            BadApplication.getInstance().db.branchDao().insertAll(it)
        }
    }

    private fun saveUser(user : User?){
        user?.let {
            BadApplication.getInstance().db.userDao().insert(user)
            run loop@{
                user.userRoleList.forEach {
                    if (it.role.id == 1 || it.role.id == 2) {
                        val sharedPrferences =
                            PreferenceManager.getDefaultSharedPreferences(BadApplication.getInstance().applicationContext)
                        val editor = sharedPrferences.edit()
                        editor.putBoolean("admin", true)
                        editor.putInt("branchId",user.branch.id?:-1)
                        editor.apply()
                        return@loop
                    }
                }
            }
        }
    }
}