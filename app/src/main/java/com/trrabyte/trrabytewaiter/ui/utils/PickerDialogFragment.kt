package com.trrabyte.trrabytewaiter.ui.utils

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.io.response.Branch
import com.trrabyte.trrabytewaiter.ui.utils.adapters.BranchPickerAdapter

class PickerDialogFragment(var lst : ArrayList<Branch>) : DialogFragment() {

//    var lst : ArrayList<Branch> = arrayListOf()
    var v : View? = null
    lateinit var sp_branch : Spinner
    var onBranchSelected : ((Branch)->Unit?)? = null

    companion object{
//        fun newIntance(branch : ArrayList<Branch>) : PickerDialogFragment{
//            var dialog = PickerDialogFragment()
//            dialog.lst = branch
//            return dialog
//        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = requireActivity().window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.transparent_d )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        v = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_picker,null,false)
        isCancelable = false
        v?.let {
            sp_branch = it.findViewById(R.id.sp_branch)
        }

        val builder = AlertDialog.Builder(requireContext(),R.style.transparentDialog)
        builder.setView(v)
            .setCancelable(false)
        val customDialog = builder.create()

        customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        customDialog.window?.decorView?.systemUiVisibility= View.SYSTEM_UI_FLAG_FULLSCREEN
        return customDialog!!
    }

    override fun onResume() {
        super.onResume()
        sp_branch.adapter = BranchPickerAdapter(requireContext(),lst)
            .setOnItemClickListener {
                onBranchSelected?.invoke(it)
                dismiss()
            }
    }
    fun setOnBranchSelected(lmda: (Branch) -> Unit?) : PickerDialogFragment{
        onBranchSelected = lmda
        return this
    }
}