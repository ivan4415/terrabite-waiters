package com.trrabyte.trrabytewaiter.ui.utils.scanner;/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.trrabyte.trrabytewaiter.R;
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.CameraSource;
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.CameraSourcePreview;
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.GraphicOverlay;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//import com.google.android.gms.vision.CameraSource;


/**
 * Activity for the multi-tracker app.  This app detects barcodes and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and ID of each mBarcode.
 */
public final class BarcodeCameraActivity extends AppCompatActivity {
    // constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String BarcodeObject = "Barcode";
    private static final String TAG = "Barcode-reader";
    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    boolean mStopHandler;
//    @BindView(R.id.image_view_clear)
//    ImageView mImageViewClear;
//    @BindView(R.id.image_view_back)
//    ImageView mImageViewBack;
//    @BindView(R.id.image_view_flash)
//    ImageView mImageViewFlash;
//    @BindView(R.id.text_view_scan)
//    TextView text_view_scan;
//    @BindView(R.id.textView2)
//    TextView text_view_sub;
//    @BindView(R.id.edit_text_reference)
//    TextView edit_text_reference;
//    @BindView(R.id.ll_toolbar_person)
//    LinearLayout linearLayoutBrackets;
//    @BindView(R.id.camera_container)
//    RelativeLayout camera_container;
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private Handler mHandler;
    private Handler mHandlerClose;
    private Barcode mBarcode;
    private Runnable mRunnable;
    private Runnable runnableClose;
    private boolean isFlashOn;
    private boolean isPayBillsFlow;
    private int maxLength;
    private int minLength;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        ThemeUtils.INSTANCE.onActivitySetThemeOfSO(this);
    }

    /**
     * Initializes the UI and creates the detector pipeline.
     */



//    @OnClick(R.id.ic_back)
//    public void finishActivity(View view) {
////        new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG22);
//        setResult(CommonStatusCodes.CANCELED, null);
//        finish();
//    }

    @Override
    public void onBackPressed() {
        setResult(CommonStatusCodes.CANCELED, null);
        finish();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        final int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            final Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (final IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {

            case RC_HANDLE_CAMERA_PERM:
                if (grantResults == null || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                } else {
                    final boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
                    final boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
//                    new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG19);
//                    new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG20);
                    createCameraSource(true, useFlash);
                }
                break;

        }

    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the mBarcode detector to detect small barcodes
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        final Context context = getApplicationContext();

        // A mBarcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the mBarcode detection results, track the barcodes, and maintain
        // graphics for each mBarcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each mBarcode.
        final BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();

        final BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the mBarcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            final IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            final boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
//                Toast.makeText(this, R.string.scanner_error_low_storage, Toast.LENGTH_LONG).show();
            }
        }


        mCameraSource =
                new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setRequestedPreviewSize(1280, 1024)
                        .setRequestedFps(2.0f)
                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null)
                        .build();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
//        ThemeUtils.INSTANCE.onActivityCreateSetTheme(this);

        setContentView(R.layout.activity_barcode_camera);
//        new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG21);
        final Bundle bundle;
        bundle = getIntent().getExtras();
        ButterKnife.bind(this);
        isPayBillsFlow = false;
//        Utils.Companion.changeColotrStatusBar(this, R.color.caribbean_green_sb, 0, false);

//        text_view_sub = findViewById(R.id.text_view_scan);
//        linearLayoutBrackets = findViewById(R.id.ll_toolbar_person);
//        if (bundle != null) {
//            if (!TextUtils.isEmpty(bundle.getString("SUB_TITLE_PAY"))) {
//                text_view_sub.setText(null);
//                text_view_sub.setText(bundle.getString("SUB_TITLE_PAY"));
//            }
////            if (bundle.getInt("COLOR_TB") != 0) {
////                linearLayoutBrackets.setBackgroundColor(ContextCompat.getColor(this, BadApplication.currentThemePosition==0? bundle.getInt("COLOR_TB"):R.color.dark_one));
////                Utils.Companion.changeColotrStatusBar(this, bundle.getInt("COLOR_SB"), 0, false);
////            }
//        }

        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.face_overlay);

        final boolean autoFocus = true;
        final boolean useFlash = false;

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        final int rc = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);
        } else {
            String[] permissions = {Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
        }

        mHandler = new Handler();
        mHandlerClose = new Handler();

        mRunnable = new Runnable() {
            @Override
            public void run() {
                codeValidator();
                if (!mStopHandler) {
                    mHandler.postDelayed(this, 500);
                }
            }
        };

        runnableClose = this::returnResult;

        mHandler.post(mRunnable);

    }


    private void codeValidator() {
        final BarcodeGraphic graphic = mGraphicOverlay.getFirstGraphic();
        mBarcode = null;
        if (graphic != null) {
            mBarcode = graphic.getBarcode();
            returnResult();
//            if (mBarcode != null) {
//                mStopHandler = true;
//                //Log.d("Account", "codeValidator: " + mBarcode.displayValue);
//                if (isPayBillsFlow) {
//
//                    if (!mBarcode.displayValue.contains("http://")) {
//                        /*     if ((mBarcode.displayValue.length() >= minLength) && (mBarcode.displayValue.length() <= maxLength)) {*/
//                        edit_text_reference.setText(mBarcode.displayValue);
////                        new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG23);
//                        /*}*/
//                    }
//                    returnResult();
//                } else {
//                    setResult(RESULT_OK, new Intent()
//                            .putExtra("result", mBarcode.displayValue));
//                    Log.d("AccountSummary", "codeValidator: " + mBarcode.displayValue);
//                    setResult(RESULT_OK, new Intent()
//                            .putExtra("result", mBarcode.displayValue));
//
//                    finish();
//                }
//
//                mHandlerClose.postDelayed(runnableClose, 1000);
//            } else {
//                Log.d(TAG, "mBarcode data is null");
//            }
        } else {
            Log.d(TAG, "no mBarcode detected");
        }
        //mPreview.stop();
    }

    private void toggleFlash() {
        if (isFlashOn) {
            mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        } else {
            mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        }


        try {
            //mImageViewFlash.setImageResource(isFlashOn ? R.drawable.ic_flash_on : R.drawable.ic_flash_off);

            isFlashOn = !isFlashOn;
        } catch (final Exception e) {
//            LogUtils.e(getClass().getSimpleName(), e.getMessage(), e.getCause());
        }

    }

    public void returnResult() {
//        if (!mBarcode.displayValue.contains("http://")) {
            // if ((mBarcode.displayValue.length() >= minLength) && (mBarcode.displayValue.length() <= maxLength)) {*/
            final Intent data = new Intent();
            data.putExtra("barcode", mBarcode);
            setResult(RESULT_OK, data);
            finish();
//        }

        /* }*/
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }

        mHandler.removeCallbacks(mRunnable);
        mHandlerClose.removeCallbacks(runnableClose);
    }

    /**
     * onTap is called to capture the oldest mBarcode currently detected and
     * return it to the caller.
     *
     * @param rawX - the raw position of the tap
     * @param rawY - the raw position of the tap.
     * @return true if the activity is ending.
     */
    private boolean onTap(float rawX, float rawY) {

        //TODO: use the tap position to select the mBarcode.
        final BarcodeGraphic graphic = mGraphicOverlay.getFirstGraphic();
        Barcode barcode = null;
        if (graphic != null) {
            barcode = graphic.getBarcode();
            if (barcode != null) {
                final Intent data = new Intent();
                data.putExtra(BarcodeObject, barcode);
                setResult(CommonStatusCodes.SUCCESS, data);
                finish();
            } else {
                Log.d(TAG, "mBarcode data is null");
            }
        } else {
            Log.d(TAG, "no mBarcode detected");
        }
        return barcode != null;
    }

    public void setFlash(View view) {
    }

    private class CaptureGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            return onTap(e.getRawX(), e.getRawY()) || super.onSingleTapConfirmed(e);
        }
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            //mCameraSource.doZoom(detector.getScaleFactor());
        }
    }
}
