package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.User
import com.trrabyte.trrabytewaiter.ui.login.LoginActivity
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import kotlinx.android.synthetic.main.fragment_menu.*

class FragmentMenu : BaseFragment<MainViewModel>(MainViewModel::class.java) {

    companion object{
        fun newInstance(showMenu : Boolean) : FragmentMenu{
            val fragment = FragmentMenu()
            fragment.showMenu = showMenu
            return fragment
        }
    }

    var showMenu = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_menu,container,false)
        return v
    }

    override fun setObservers() {
        super.setObservers()
        var user = fragmentViewModel?.getUser()
        setData(user)
    }

    private fun setData(user : User?){
        user?.let {
            tvName.text = it.firstName
            tvLast.text =it.lastName
        }
    }

    override fun setListeners() {
        super.setListeners()
        tvCerrarSesion.setOnClickListener(this)
        if(showMenu) {
            ic_tables.setOnClickListener(this)
            ic_qr.setOnClickListener(this)
            ic_history.setOnClickListener(this)
        }else{
            ic_tables.visibility = View.GONE
            ic_qr.visibility = View.GONE
            ic_history.visibility = View.GONE
        }
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.tvCerrarSesion->{
                fragmentViewModel?.closeSession()
                requireActivity().startActivity(Intent(requireActivity(),LoginActivity::class.java))
            }

            R.id.ic_tables->{
                fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_ORDERS)
            }

            R.id.ic_qr->{
                fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_QR)
            }

            R.id.ic_history->{
                fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_HISTORY)
            }
        }
    }



}
