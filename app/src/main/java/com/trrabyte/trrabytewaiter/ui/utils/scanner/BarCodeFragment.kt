package com.trrabyte.trrabytewaiter.ui.utils.scanner

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.gson.Gson
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.request.QrRequest
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentMenu
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.CameraSource
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.CameraSourcePreview
import com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.GraphicOverlay
import kotlinx.android.synthetic.main.activity_barcode_camera.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.IOException
import java.nio.charset.StandardCharsets

class BarCodeFragment : BaseFragment<MainViewModel>(MainViewModel::class.java) {

    companion object {
        const val AutoFocus = "AutoFocus"
        const val UseFlash = "UseFlash"
        const val BarcodeObject = "Barcode"
        private const val TAG = "Barcode-reader"

        // intent request code to handle updating play services if needed.
        private const val RC_HANDLE_GMS = 9001

        // permission request codes need to be < 256
        private const val RC_HANDLE_CAMERA_PERM = 2
    }

    var mStopHandler = false

    var mImageViewClear: ImageView? = null

    var linearLayoutBrackets: LinearLayout? = null

    var camera_container: RelativeLayout? = null

    private var mCameraSource: com.trrabyte.trrabytewaiter.ui.utils.scanner.camera.CameraSource? =
        null
    private var mPreview: CameraSourcePreview? = null
    private var mGraphicOverlay: GraphicOverlay<BarcodeGraphic>? = null
    private var mHandler: android.os.Handler? = null
    private var mHandlerClose: android.os.Handler? = null
    private var mBarcode: Barcode? = null
    private var mRunnable: Runnable? = null
    private var runnableClose: Runnable? = null
    private var isFlashOn = false
    private var isPayBillsFlow = false
    private val maxLength = 0
    private val minLength = 0
    var tables = ArrayList<NewBaseResponse>()

    override fun onPause() {
        super.onPause()
        mPreview?.stop()
        if (mPreview != null) {
            mPreview!!.release()
        }

        mHandler!!.removeCallbacks(mRunnable!!)
        mHandlerClose!!.removeCallbacks(runnableClose!!)
    }

//    override fun onStop() {
//        super.onStop()
//        mPreview?.stop()
//        if (mPreview != null) {
//            mPreview!!.release()
//        }
//
//        mHandler!!.removeCallbacksAndMessages(null)
//        mHandlerClose!!.removeCallbacks(runnableClose!!)
//    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    @Throws(SecurityException::class)
    private fun startCameraSource() {
        // check that the device has play services available.
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
            requireActivity().applicationContext
        )
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance()
                .getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg?.show()
        }
        if (mCameraSource != null) {
            try {
                mPreview!!.start(mCameraSource, mGraphicOverlay)
            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                mCameraSource?.release()
                mCameraSource = null
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            RC_HANDLE_CAMERA_PERM -> if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                requireActivity().onBackPressed()
            } else {
                val autoFocus: Boolean =
                    requireArguments().getBoolean(AutoFocus, true)
                val useFlash: Boolean =
                    requireArguments().getBoolean(BarcodeCameraActivity.UseFlash, false)
                createCameraSource(true, useFlash)
            }
        }
    }

    private fun createCameraSource(autoFocus: Boolean, useFlash: Boolean) {
        val context: Context = requireActivity().applicationContext

        // A mBarcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the mBarcode detection results, track the barcodes, and maintain
        // graphics for each mBarcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each mBarcode.
        val barcodeDetector = BarcodeDetector.Builder(context).build()
        val barcodeFactory = BarcodeTrackerFactory(mGraphicOverlay)
        barcodeDetector.setProcessor(
            MultiProcessor.Builder(barcodeFactory).build()
        )
        if (!barcodeDetector.isOperational) {
            // Note: The first time that an app using the mBarcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.")

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            val lowstorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = requireActivity().registerReceiver(null, lowstorageFilter) != null
            if (hasLowStorage) {
//                Toast.makeText(this, R.string.scanner_error_low_storage, Toast.LENGTH_LONG).show();
            }
        }
        mCameraSource = CameraSource.Builder(requireActivity().applicationContext, barcodeDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 1024)
            .setRequestedFps(2.0f)
            .setFlashMode(if (useFlash) Camera.Parameters.FLASH_MODE_TORCH else null)
            .setFocusMode(if (autoFocus) Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE else null)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.activity_barcode_camera, container, false)
        isPayBillsFlow = false
        //        Utils.Companion.changeColotrStatusBar(this, R.color.caribbean_green_sb, 0, false);

//        text_view_sub = findViewById(R.id.text_view_scan);
//        linearLayoutBrackets = findViewById(R.id.ll_toolbar_person);
//        if (bundle != null) {
//            if (!TextUtils.isEmpty(bundle.getString("SUB_TITLE_PAY"))) {
//                text_view_sub.setText(null);
//                text_view_sub.setText(bundle.getString("SUB_TITLE_PAY"));
//            }
////            if (bundle.getInt("COLOR_TB") != 0) {
////                linearLayoutBrackets.setBackgroundColor(ContextCompat.getColor(this, BadApplication.currentThemePosition==0? bundle.getInt("COLOR_TB"):R.color.dark_one));
////                Utils.Companion.changeColotrStatusBar(this, bundle.getInt("COLOR_SB"), 0, false);
////            }
//        }
        mPreview = v.findViewById(R.id.preview)
        mGraphicOverlay = v.findViewById(R.id.face_overlay)
        val autoFocus = true
        val useFlash = false
        val rc = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash)
        } else {
            val permissions = arrayOf(Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(
                requireActivity(),
                permissions,
                RC_HANDLE_CAMERA_PERM
            )
        }

        startHandlers()
        return v
    }

    private fun startHandlers() {
        mHandler = Handler()
        mHandlerClose = Handler()
        mRunnable = object : Runnable {
            override fun run() {
                codeValidator()
                if (!mStopHandler) {
                    mHandler?.postDelayed(this, 500)
                }
            }
        }
        runnableClose = Runnable { this.returnResult() }
        mHandler?.post(mRunnable!!)
    }

    private fun codeValidator() {
        val graphic = mGraphicOverlay!!.firstGraphic
        mBarcode = null
        if (graphic != null) {
            mBarcode = graphic.barcode
            returnResult()
            /*
            //            if (mBarcode != null) {
//                mStopHandler = true;
//                //Log.d("Account", "codeValidator: " + mBarcode.displayValue);
//                if (isPayBillsFlow) {
//
//                    if (!mBarcode.displayValue.contains("http://")) {
//                        /*     if ((mBarcode.displayValue.length() >= minLength) && (mBarcode.displayValue.length() <= maxLength)) {*/
//                        edit_text_reference.setText(mBarcode.displayValue);
////                        new CashOutTrackerImpl(this).trackCashOutEvent(CashOutTracker.CashOutEvent.CASHOUT_EVENT_TAG23);
//                        /*}*/
//                    }
//                    returnResult();
//                } else {
//                    setResult(RESULT_OK, new Intent()
//                            .putExtra("result", mBarcode.displayValue));
//                    Log.d("AccountSummary", "codeValidator: " + mBarcode.displayValue);
//                    setResult(RESULT_OK, new Intent()
//                            .putExtra("result", mBarcode.displayValue));
//
//                    finish();
//                }
//
//                mHandlerClose.postDelayed(runnableClose, 1000);
//            } else {
//                Log.d(TAG, "mBarcode data is null");
//            }

             */
        } else {
            Log.d(TAG, "no mBarcode detected")
        }
        //mPreview.stop();
    }

    private fun toggleFlash() {
        if (isFlashOn) {
            mCameraSource!!.setFlashMode(Camera.Parameters.FLASH_MODE_OFF)
        } else {
            mCameraSource!!.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH)
        }
        try {
            //mImageViewFlash.setImageResource(isFlashOn ? R.drawable.ic_flash_on : R.drawable.ic_flash_off);
            isFlashOn = !isFlashOn
        } catch (e: Exception) {
//            LogUtils.e(getClass().getSimpleName(), e.getMessage(), e.getCause());
        }
    }

    fun returnResult() {
//        if (!mBarcode.displayValue.contains("http://")) {
        // if ((mBarcode.displayValue.length() >= minLength) && (mBarcode.displayValue.length() <= maxLength)) {*/
//        Resultado del escaneo
//        val data = Bundle()
//        data.putString("barcode", mBarcode?.displayValue?:"")
        mHandler!!.removeCallbacks(mRunnable!!)
        mHandlerClose!!.removeCallbacks(runnableClose!!)
        mStopHandler = true
        mPreview?.release()
        var decodeString =
            String(Base64.decode(mBarcode?.displayValue, Base64.DEFAULT), StandardCharsets.UTF_8)
        val requ = Gson().fromJson(decodeString, QrRequest::class.java)
        showProgress()
        fragmentViewModel?.getQrOrder(requ)?.observe(viewLifecycleOwner, Observer {
            if (it.hasError) {
                Utils.showSimpleDialog(
                    requireContext(),
                    message = it.message,
                    positiveListener = DialogInterface.OnClickListener { d, _ ->
                        createCameraSource(true, false)
                        mPreview = v.findViewById(R.id.preview)
                        startCameraSource()
                        startHandlers()
                        d.dismiss()
                        onResume()
                    })
            } else {
                if (it.data?.state == "occupied") {
                    setTables(it.data?.order?.tableNumber ?: 1)
                    val b = Bundle()
                    b.putInt("id", it.data?.order?.id ?: -1)
                    b.putSerializable("tables", tables)
                    navigateToFragment(R.id.barCode_to_add, b)
                    Utils.showToast("Ocupada")
                } else {
                    setTables(it.data?.tableNum?:1)
                    val b = Bundle()
                    b.putInt("numOfTables", it.data?.tableNum ?: 1)
                    b.putSerializable("tables", tables)
                    b.putBoolean("isFromQR", true)
                    navigateToFragment(R.id.barCode_to_add, b)
                }
            }
            hideProgress()
        })
    }

    private fun setTables(numOfTables: Int) {
        for (i in 0..numOfTables) {
            val table = NewBaseResponse()
            table.id = i
            table.name = "M-${i + 1}"
            tables.add(table)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mPreview != null) {
            mPreview!!.release()
        }

        mHandler!!.removeCallbacksAndMessages(null)
        mHandlerClose!!.removeCallbacks(runnableClose!!)
    }

    override fun setListeners() {
        super.setListeners()
        setDrawerView()
        ic_menu.visibility = View.VISIBLE
        ic_menu.setOnClickListener(this)
    }

    override fun setObservers() {
        super.setObservers()
        fragmentViewModel?.getFragment()?.observe(viewLifecycleOwner, Observer {
            drawer_layout.closeDrawer(GravityCompat.START)
            when(it?:MainViewModel.FragmentsMenu.DEFAULT){
                MainViewModel.FragmentsMenu.FRAGMENT_ORDERS -> {
                    navigateToFragment(R.id.qr_to_fragment_main)
                    fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
                MainViewModel.FragmentsMenu.FRAGMENT_QR -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
                MainViewModel.FragmentsMenu.FRAGMENT_HISTORY -> {
                    navigateToFragment(R.id.qr_to_fragment_history)
                    fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
                MainViewModel.FragmentsMenu.DEFAULT -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.ic_menu->{
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun setDrawerView() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.ll_drawer_start, FragmentMenu(), "")
            .disallowAddToBackStack()
            .commitAllowingStateLoss()
    }
}