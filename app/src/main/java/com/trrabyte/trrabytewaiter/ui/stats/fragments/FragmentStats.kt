package com.trrabyte.trrabytewaiter.ui.stats.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.StadisticsData
import com.trrabyte.trrabytewaiter.io.response.StadisticsResponse
import com.trrabyte.trrabytewaiter.ui.main.fragments.FragmentMenu
import com.trrabyte.trrabytewaiter.ui.stats.data.StatsViewModel
import com.trrabyte.trrabytewaiter.ui.utils.PickerDialogFragment
import kotlinx.android.synthetic.main.fragment_stats.*
import kotlinx.android.synthetic.main.toolbar.*

class FragmentStats : BaseFragment<StatsViewModel>(StatsViewModel::class.java) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_stats, container, false)
        return v
    }

    override fun setObservers() {
        super.setObservers()
        ic_menu.visibility = View.VISIBLE
        ic_menu.setOnClickListener(this)
        showProgress()
        fragmentViewModel?.getStats(requireActivity().intent.getIntExtra("branchId", -1))
            ?.observe(viewLifecycleOwner, Observer {
                if (it.hasError) {

                    Utils.showToast("error")
                } else {
                    it.data?.let { data ->
                        fragmentViewModel?.saveBranch(data.branchList)
                        setData(data)
                        Utils.showToast("success")
                    }
                }
                hideProgress()
            })
    }

    override fun setListeners() {
        super.setListeners()
        setDrawerView()
    }

    private fun setPieGraph(lst: ArrayList<StadisticsData>, pieGraph: PieChart) {
        val pieData = arrayListOf<PieEntry>()
        val colors = arrayListOf<Int>()
        lst.forEach {
            pieData.add(PieEntry(it.totalAmount?.toFloat() ?: -1f, it.name))
            colors.add(Color.parseColor(it.color ?: "red"))
        }
        val dataSet = PieDataSet(pieData, "")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 1f
        dataSet.colors = colors

        pieGraph.setUsePercentValues(true)
        pieGraph.isRotationEnabled = false
        pieGraph.data = PieData(dataSet)
        pieGraph.description.isEnabled = false
        pieGraph.animateX(1400, Easing.EaseInOutQuad)
        pieGraph.invalidate()
    }

    private fun setBarGraph(lst: ArrayList<StadisticsData>, barGraph: BarChart) {
        var colorsMock = arrayListOf<String>(
            "red",
            "blue",
            "green",
            "black",
            "white",
            "gray",
            "cyan",
            "magenta",
            "yellow",
            "lightgray",
            "darkgray",
            "grey",
            "lightgrey",
            "darkgrey",
            "aqua",
            "fuchsia",
            "lime",
            "maroon",
            "navy",
            "olive",
            "purple",
            "silver",
            "teal"
        )
        val barData = arrayListOf<BarEntry>()
        val colors = arrayListOf<Int>()
        var i = 0
        val tags = arrayListOf<String>()

        lst.forEach {
            barData.add(
                BarEntry(
                    it.monthNumber?.toFloat() ?: -1f,
                    it.totalAmount?.toFloat() ?: -1f,
                    "Enero"
                )
            )
            colors.add(Color.parseColor(colorsMock[i]))
            tags.add(it.name ?: "")
            i++
        }

        val barDataSet = BarDataSet(barData, "Test")
        val dataSets = arrayListOf<IBarDataSet>()
        barDataSet.colors = colors

        dataSets.add(barDataSet)
        val data = BarData(dataSets)
        data.barWidth = 1f

        barGraph.data = data
        barGraph.description.isEnabled = false
        val xAxis = barGraph.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.valueFormatter = MyAxisFormatter(tags)
        xAxis.setDrawLabels(true)
        xAxis.granularity = 1f

        barGraph.invalidate()
    }

    inner class MyAxisFormatter(val lst: ArrayList<String>) : IndexAxisValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if (index < lst.size) {
                lst[index]
            } else {
                ""
            }
        }
    }

    private fun setData(data: StadisticsResponse) {
//        var pieData = arrayListOf<PieEntry>()
//        var bardata = arrayListOf<BarEntry>()
//
        setPieGraph(data.paymentMethodList!!, gr_paiment_method)
        setPieGraph(data.platformList!!, gr_device)
        setBarGraph(data.montlySalesList!!, gr_months)

        /*
       //
       //        for(i in 1..3){
       //            val temp : Float = (((Math.random()*i)+i)/5).toFloat()
       //            pieData.add(PieEntry(temp,"Parte $i",R.drawable.logo))
       //            bardata.add(BarEntry(i.toFloat(),temp,R.drawable.logo))
       //        }
       //
       //        val dataSet = PieDataSet(pieData,"test")
       //        dataSet.setDrawIcons(false)
       //        dataSet.sliceSpace = 3f
       //        dataSet.selectionShift = 5f
       //        val colors = arrayListOf<Int>()
       //        for (c in ColorTemplate.JOYFUL_COLORS)
       //            colors.add(c)
       //
       //        dataSet.setColors(colors)
       ////        val data = PieData(dataSet)
       ////        gr_paiment_method.data = data
       ////        gr_paiment_method.invalidate()
       ////        gr_device.data = data
       ////        gr_device.invalidate()*/
/*
        val barDataSetL = BarDataSet(bardata,"Prueba Barras")
        val barDataSet : ArrayList<BarDataSet> = arrayListOf()

        var i = 0

        bardata.forEach{
            var newDataSet = BarDataSet(arrayListOf(it),i.toString())
//            newDataSet.color = ContextCompat.getColor(requireContext(),android.R.color.black)
            newDataSet.color = Color.parseColor("#02E623")
            barDataSet.add(newDataSet)
            barDataSet
            i++
        }

        val dataSets =  arrayListOf<IBarDataSet>()

        barDataSet.forEach{
            dataSets.add(it)
        }
//        dataSets.add(barDataSet)
        val barData = BarData(dataSets)
        barData.barWidth = 0.2f

        gr_months.data = barData
        gr_months.invalidate()*/
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_branch -> {
                PickerDialogFragment(fragmentViewModel?.getBranch() ?: arrayListOf())
                    .setOnBranchSelected {
                        fragmentViewModel?.setBranchId(it.id ?: -1)
                        tv_branch.text = it.name!!.split("-")[1]
                        Unit
                    }
                    .show(parentFragmentManager, "SHOW")
            }
            R.id.ic_menu -> {
//                Menú lateral
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun setDrawerView() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.ll_drawer_start, FragmentMenu.newInstance(false), "")
            .disallowAddToBackStack()
            .commitAllowingStateLoss()
    }
}