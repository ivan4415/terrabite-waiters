package com.trrabyte.trrabytewaiter.ui.stats.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.ui.BaseRepository
import com.trrabyte.trrabytewaiter.io.BadApplication
import com.trrabyte.trrabytewaiter.io.request.StadisticsRequest
import com.trrabyte.trrabytewaiter.io.response.StadisticsResponse
import com.trrabyte.trrabytewaiter.ui.login.data.LoginRepository

class StatsRepository : BaseRepository() {

    fun getStats(request : StadisticsRequest) : LiveData<BaseResponse<StadisticsResponse>>{
        val res = MutableLiveData<BaseResponse<StadisticsResponse>>()
        BadApplication.getInstance().controllerWaiters.getStadistics(request).service(res)
        return res
    }


}