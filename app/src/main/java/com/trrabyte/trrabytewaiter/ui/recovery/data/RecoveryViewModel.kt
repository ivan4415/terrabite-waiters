package com.trrabyte.trrabytewaiter.ui.recovery.data

import android.app.Application
import androidx.lifecycle.LiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.ui.BaseViewModel
import com.trrabyte.trrabytewaiter.io.request.RecoveryRequest
import com.trrabyte.trrabytewaiter.io.request.ResetPasswordRequest
import com.trrabyte.trrabytewaiter.io.request.VerifyCodeRequest
import com.trrabyte.trrabytewaiter.io.response.UserRole

class RecoveryViewModel(application: Application) : BaseViewModel(application) {

    val repository : RecoveryRepository by lazy { RecoveryRepository() }

    fun sendEmail(email : String) : LiveData<BaseResponse<String>>{
        var request = RecoveryRequest()
        request.email = email
        return  repository.sendEmail(request)
    }

    fun sendCode(code : String) : LiveData<BaseResponse<UserRole>>{
        var request = VerifyCodeRequest()
        request.code = code
        return  repository.sendVerificationCode(request)
    }

    fun changePassword(userID : Int, pass : String) : LiveData<BaseResponse<String>>{
        var request = ResetPasswordRequest()
        request.userId = userID
        request.password = pass
        request.confirm = pass
        return repository.changePassword(request)
    }

}