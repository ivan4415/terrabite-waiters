package com.trrabyte.trrabytewaiter.ui.recovery.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.ui.recovery.data.RecoveryViewModel
import kotlinx.android.synthetic.main.fragment_recovery_email.*

class FragmentChangePassword : BaseFragment<RecoveryViewModel>(RecoveryViewModel::class.java) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_recovery_email,container,false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ti_confirm.visibility = View.VISIBLE
        ti_email.hint = "Contraseña"
    }

    override fun setListeners() {
        super.setListeners()
        btn_continue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.btn_continue->{
                showProgress()
                fragmentViewModel?.changePassword(requireArguments().getInt("userId",-1),et_confirm.text.toString())
                    ?.observe(viewLifecycleOwner, Observer {
                        if(it.hasError){
                            Utils.showSimpleDialog(requireContext(),it.message)
                        }else{
                            requireActivity().finish()
                        }
                        hideProgress()
                    })
            }
        }
    }

}