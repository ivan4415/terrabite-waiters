package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.utils.BaseAdapter
import com.trrabyte.trrabytewaiter.io.response.Orders
import org.simpleframework.xml.Order

class OrdersAdapter (var ctx : Context,lst : ArrayList<Orders>) : BaseAdapter<OrdersAdapter.OrdersViewHolder,Orders>(lst){

    var btnClickListener : ((Orders)->Unit)? = null
    var btnCancelClickListener : ((Orders)->Unit)? = null

    fun setBtnClickListener(lmda : (Orders)->Unit) : OrdersAdapter{
        btnClickListener = lmda
        return this
    }
    fun setBtnCancelClickListener(lmda : (Orders)->Unit) : OrdersAdapter{
        btnCancelClickListener = lmda
        return this
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersViewHolder {
        return OrdersViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_orders,parent,false))
    }

    override fun onBindViewHolder(holder: OrdersViewHolder, position: Int) {
        val item = lst[position]
        holder.tv_num_table.text = item.tableNumber.toString()
        holder.tv_cost.text = "$ ${String.format("%.2f",item.totalAmount)}"
        holder.tv_num_dish.text = item.orderDetailList.size.toString()
        holder.btn_check.setOnClickListener{
            btnClickListener?.invoke(item)
        }
        holder.cl_main.setOnClickListener{
            itemClickListener?.invoke(item)
        }
        holder.btn_cancel.setOnClickListener{
            btnCancelClickListener?.invoke(item)
        }
    }

    class OrdersViewHolder (itemView : View): BaseAdapter.BaseViewHolder(itemView){
        val tv_num_table :TextView = itemView.findViewById(R.id.tv_num_table)
        val tv_cost : TextView = itemView.findViewById(R.id.tv_cost)
        val tv_num_dish : TextView = itemView.findViewById(R.id.tv_num_dish)
        val btn_check : Button = itemView.findViewById(R.id.btn_check)
        val cl_main : ConstraintLayout = itemView.findViewById(R.id.cl_main)
        val btn_cancel : Button = itemView.findViewById(R.id.btn_cancel)
    }
}
