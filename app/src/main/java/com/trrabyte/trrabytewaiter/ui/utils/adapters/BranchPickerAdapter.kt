package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.io.response.Branch

class BranchPickerAdapter(var ctx : Context,var lst : ArrayList<Branch>) : BaseAdapter(){

    var onItemClickListener : ((Branch) ->Unit?)? =  null

    fun setOnItemClickListener(lmda: (Branch) -> Unit?) : BranchPickerAdapter{
        onItemClickListener = lmda
        return this
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val v = LayoutInflater.from(ctx).inflate(R.layout.item_spiner_branch,parent,false)
        val viewHolder = BranchViewHolder(v)
        viewHolder.branchName.text = lst[position].name
        viewHolder.branchName.setOnClickListener{
            onItemClickListener?.invoke(lst[position])
        }
        return v
    }

    internal inner class BranchViewHolder(view: View){
        var branchName : TextView = view.findViewById(R.id.tv_branch_name)

    }

    override fun getItem(position: Int): Any {
        return lst[position]
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    override fun getCount(): Int {
        return lst.size
    }
}