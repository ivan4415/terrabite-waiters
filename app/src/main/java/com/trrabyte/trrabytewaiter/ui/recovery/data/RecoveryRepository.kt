package com.trrabyte.trrabytewaiter.ui.recovery.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.SUCCESS
import com.trrabyte.trrabytewaiter.base.io.BaseResponse.Companion.setResponseError
import com.trrabyte.trrabytewaiter.base.ui.BaseRepository
import com.trrabyte.trrabytewaiter.io.BadApplication
import com.trrabyte.trrabytewaiter.io.request.RecoveryRequest
import com.trrabyte.trrabytewaiter.io.request.ResetPasswordRequest
import com.trrabyte.trrabytewaiter.io.request.VerifyCodeRequest
import com.trrabyte.trrabytewaiter.io.response.UserRole
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecoveryRepository : BaseRepository() {

    fun sendEmail(emailRequest : RecoveryRequest) : LiveData<BaseResponse<String>>{
        val res = MutableLiveData<BaseResponse<String>>()
        BadApplication.getInstance().controllerWaiters.sendEmail(emailRequest).enqueue(object : Callback<BaseResponse<String>>{
            override fun onResponse(
                call: Call<BaseResponse<String>>,
                response: Response<BaseResponse<String>>
            ) {
                response.body()?.run{
                    when(codigoOperacion){
                        SUCCESS->{
                            res.value = this
                        }
                        else->{
                            res.value = setResponseError(this.message)
//                            setErrorMessage(message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }

        })
        return res
    }

    fun sendVerificationCode(code : VerifyCodeRequest) : LiveData<BaseResponse<UserRole>>{
        val res = MutableLiveData<BaseResponse<UserRole>>()
        BadApplication.getInstance().controllerWaiters.verifyCode(code).enqueue(object : Callback<BaseResponse<UserRole>>{
            override fun onResponse(
                call: Call<BaseResponse<UserRole>>,
                response: Response<BaseResponse<UserRole>>
            ) {
                response.body()?.run {
                    when(codigoOperacion){
                        SUCCESS->{
                            res.value = this
                        }
                        else->{
                            res.value = setResponseError(message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse<UserRole>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }
        })
        return res
    }

    fun changePassword(req : ResetPasswordRequest) : LiveData<BaseResponse<String>>{
        val res = MutableLiveData<BaseResponse<String>>()
        BadApplication.getInstance().controllerWaiters.resetPassword(req).enqueue(object : Callback<BaseResponse<String>>{
            override fun onResponse(
                call: Call<BaseResponse<String>>,
                response: Response<BaseResponse<String>>
            ) {
                response.body()?.run{
                    when(codigoOperacion){
                        SUCCESS->{
                            res.value = this
                        }
                        else->{
                            res.value = setResponseError(message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                res.value = setResponseError(t.message)
            }

        })
        return res
    }
}