package com.trrabyte.trrabytewaiter.ui.stats.data

import android.app.Application
import androidx.lifecycle.LiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.ui.BaseViewModel
import com.trrabyte.trrabytewaiter.io.BadApplication
import com.trrabyte.trrabytewaiter.io.request.StadisticsRequest
import com.trrabyte.trrabytewaiter.io.response.Branch
import com.trrabyte.trrabytewaiter.io.response.StadisticsResponse
import com.trrabyte.trrabytewaiter.ui.login.data.LoginRepository
import com.trrabyte.trrabytewaiter.ui.main.data.MainRepository
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel

class StatsViewModel(application : Application) :  BaseViewModel(application){

    val repository by lazy { StatsRepository() }
    val loginRepository by lazy { LoginRepository() }
    val mainRepository by lazy { MainRepository() }
    private var branchId = -1

    fun getStats(branchId : Int) : LiveData<BaseResponse<StadisticsResponse>>{
        var request = StadisticsRequest()
        this.branchId = branchId
        request.branchId = branchId
        request.yearListFilter = arrayListOf()
        return repository.getStats(request)
    }

    fun saveBranch(lst : ArrayList<Branch>?){
        loginRepository.saveBranch(lst)
    }

    fun getBranch(): ArrayList<Branch> = mainRepository.getBranch()

    fun setBranchId(id: Int) {
        this.branchId = id
    }
}