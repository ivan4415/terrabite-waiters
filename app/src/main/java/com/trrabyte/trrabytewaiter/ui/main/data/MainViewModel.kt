package com.trrabyte.trrabytewaiter.ui.main.data

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import com.trrabyte.trrabytewaiter.base.ui.BaseViewModel
import com.trrabyte.trrabytewaiter.io.request.*
import com.trrabyte.trrabytewaiter.io.response.*

class MainViewModel(application: Application) : BaseViewModel(application) {

    private val repository: MainRepository by lazy { MainRepository() }
    private val car = MutableLiveData<HashMap<Int, Dish>>()
    private var mainOrder = HashMap<Int, Dish>()
    private var idOrder: Int = -1
    private var folio : Int = -1
    private var navigateToFragment = MutableLiveData<FragmentsMenu>()
    var isUpdate = false
    var updateList = HashMap<Int,Dish>()
    private val check = MutableLiveData<Double>()

    companion object {
        private var branchId: Int = -1
    }
    private val table = MutableLiveData<Int>()

    fun setMainOrder(map : Dish){
        mainOrder[map.productId!!] = map.clone()
    }

    fun getMainOrder() = mainOrder

    fun getBranchId() : Int = branchId

    fun getMenu(): LiveData<BaseResponse<NewOrderResponse>> {
        val request = OrdersRequest()
        request.branchId = branchId
        return repository.getMenu(request)
    }

    fun setBranchId(id: Int) {
        branchId = id
    }

    fun setFolio (folio : Int){
        this.folio = folio
    }

    fun getFolio() = folio

    fun getUpdateMap() : HashMap<Int,Dish> = updateList

    fun setUpdateHasMap(dish : Dish){
        if(updateList.containsKey(dish.productId)){
            updateList[dish.productId]!!.count =+ dish.count
        }else{
            updateList[dish.productId!!] = dish
        }
    }

    fun removeItemUpdate(dish : Dish){
        if(updateList.containsKey(dish.productId)){
            if(updateList[dish.productId]!!.count>1){
                updateList[dish.productId]!!.count =- 1
            }else{
                updateList.remove(dish.productId)
            }
        }
    }

    fun sendOrder(/*table : Int,*/
        lst: ArrayList<Dish>
    )
            : LiveData<BaseResponse<CreateOrderResponse>> {
        val request = CreateOrderRequest()
        request.branchId = branchId
        request.orderTypeId = "eat_here"
        request.tableNumber = table.value ?: 1
        request.orderDetailList = lst
        return repository.createOrder(request)
    }

    fun getOrders(): LiveData<BaseResponse<OrdersResponse>> {
        val request = OrdersRequest()
        request.branchId = branchId
        return repository.getOrders(request)
    }

    fun getBranch(): ArrayList<Branch> = repository.getBranch()

    fun getPaymentMethod(id: Int): LiveData<BaseResponse<GetCheckResponse>> =
        repository.getPaymentMethod(id)

    fun chargeOrder(orderId: Int, paymentMethodId: String): LiveData<BaseResponse<String>> {
        var req = ChargeOrderRequest()
        req.branchId = branchId
        req.orderId = orderId
        req.paymentMethodId = paymentMethodId
        return repository.chargeOrder(req)
    }

    fun getEditOrder(id: Int): LiveData<BaseResponse<EditOrderResponse>> {
        idOrder = id
        return repository.getEditOrder(id)
    }

    fun updateOrder(removeList : ArrayList<Int>): LiveData<BaseResponse<EditOrderResponse>> {

        val req = CreateOrderRequest()
        req.id = idOrder
        req.orderTypeId = "eat_here"
        req.tableNumber = table.value ?: 1
        req.branchId = branchId
        req.orderDetailList = ArrayList(car.value!!.values.toList())
        req.deletedDetailList = removeList
        return repository.updateOrder(req)
    }

    fun setCar(map: HashMap<Int, Dish>?) {
        car.value = map
        map?.let { setCheck(it) }
    }

    fun setTable(numTable: Int) {
        table.value = numTable
    }

    fun getTable  () : Int? = table.value

    fun getCar(): LiveData<HashMap<Int, Dish>> {
        return car
    }

    fun setCheck(car: HashMap<Int, Dish>) {
        var count = 0.0
        car.values.forEach {
            count += (it.price * it.count)
        }
        check.value = count
    }


    fun cancelOrder(id : Int) : LiveData<BaseResponse<String>>{
        val req = OrdersRequest()
        req.branchId = branchId
        req.orderId = id
        return repository.cancelOrder(req)
    }

    fun getCheck(): LiveData<Double> {
        return check
    }

    fun closeSession(){
        repository.closeSession()
    }

    fun getUser() : User{
        return repository.getUser()
    }

    fun setFragment(fragment : FragmentsMenu) {
        navigateToFragment.value = fragment
    }

    fun getFragment() = navigateToFragment

    fun getHistory(date : String) : LiveData<BaseResponse<OrdersResponse>>{
        val req = HistoryRequest()
        req.branchId = branchId
        req.date = date
        req.page = 0
        return repository.getHistory(req)
    }

    fun getQrOrder(req : QrRequest) : LiveData<BaseResponse<QrResponse>> = repository.getQrOrder(req)

    enum class FragmentsMenu{
        FRAGMENT_ORDERS{
            override fun toString(): String {
                return "orders"
            }
                       },
        FRAGMENT_QR{
            override fun toString(): String {
                return "QR"
            }
                   },
        FRAGMENT_HISTORY{
            override fun toString(): String {
                return "history"
            }
        },
        DEFAULT{
            override fun toString(): String {
                return "default"
            }
        }
    }
}