package com.trrabyte.trrabytewaiter.ui.login

import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.ui.login.data.LoginViewModel
import com.trrabyte.trrabytewaiter.base.ui.BaseActivity

class LoginActivity : BaseActivity<LoginViewModel>(R.navigation.navigation_login)