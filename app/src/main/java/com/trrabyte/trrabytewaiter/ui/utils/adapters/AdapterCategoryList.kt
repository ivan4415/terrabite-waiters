package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.utils.BaseAdapter
import com.trrabyte.trrabytewaiter.io.response.Product
import com.trrabyte.trrabytewaiter.io.response.ProductCategory

class AdapterCategoryList (val ctx : Context, lst : ArrayList<ProductCategory>):
    BaseAdapter<AdapterCategoryList.CategoryListHolder, ProductCategory>(lst) {

    var addToCar : ((Product,Int)->Unit)? = null

    fun setAddToCar(mCallback : (Product,Int)->Unit) : AdapterCategoryList{
        addToCar = mCallback
        return this
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryListHolder {
        return CategoryListHolder(LayoutInflater.from(ctx).inflate(R.layout.item_category,parent,false))
    }

    override fun onBindViewHolder(holder: CategoryListHolder, position: Int) {
        holder.tv_categoria.text = lst[position].name
        holder.rv_products.layoutManager = GridLayoutManager(ctx,2)
        val adapter = ImageAdapter(ctx,lst[position].productList)
            .setOnItemClicListener{
                addToCar?.invoke(it,lst[position].id?:-1)
            }
        holder.rv_products.adapter = adapter
    }

    class CategoryListHolder(itemView : View) : BaseAdapter.BaseViewHolder(itemView){

        val tv_categoria :TextView = itemView.findViewById(R.id.tv_categoria)
        val rv_products : RecyclerView = itemView.findViewById(R.id.rv_products)
    }
}

class ImageAdapter(val ctx : Context, lst : ArrayList<Product>):
    BaseAdapter<ImageAdapter.ProductViewHolder, Product>(lst) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_product,parent,false))
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.tv_name.text = lst[position].name
        holder.btn_agregar.setOnClickListener{
            if(holder.count>0) {
                lst[position].count = holder.count
                itemClickListener?.invoke(lst[position])
                holder.restart()
            }else{
                Utils.showToast("Se debe selecciónar más de un platillo")
            }
        }
        if(!TextUtils.isEmpty(lst[position].imageUrl)) {
            Picasso.get()
                .load(lst[position].imageUrl)
                .placeholder(R.drawable.logo)
                .error(R.drawable.logo)
                .into(holder.img_product)
        }
    }

    class ProductViewHolder(itemView : View) : BaseAdapter.BaseViewHolder(itemView){

        val img_product :ImageView = itemView.findViewById(R.id.img_product)
        val img_add : ImageView = itemView.findViewById(R.id.img_add)
        val img_remove : ImageView = itemView.findViewById(R.id.img_remove)
        val tv_count : TextView = itemView.findViewById(R.id.tv_count)
        val tv_name : TextView = itemView.findViewById(R.id.tv_name)
        val btn_agregar : Button = itemView.findViewById(R.id.btn_agregar)
        var count = 1

        init {
            setListeners()
            setCount()
        }

        private fun setListeners(){
            img_add.setOnClickListener{
                count += 1
                setCount()
            }
            img_remove.setOnClickListener {
                count -= 1
                setCount()
            }
        }

        private fun setCount(){
            img_remove.visibility = if(count>0)View.VISIBLE else View.INVISIBLE
            btn_agregar.visibility = img_remove.visibility
            tv_count.text = count.toString()
        }

        fun restart(){
            count = 1
            setCount()
        }
    }
}
