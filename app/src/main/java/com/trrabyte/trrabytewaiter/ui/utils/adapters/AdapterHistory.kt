package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.utils.BaseAdapter
import com.trrabyte.trrabytewaiter.io.response.Orders
import org.w3c.dom.Text

class AdapterHistory(var ctx : Context, lst : ArrayList<Orders>) : BaseAdapter<AdapterHistory.HistoryViewHolder,Orders> (lst){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_history,parent,false))
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.tv_table.text = "Mesa ${lst[position].tableNumber}"
        holder.tv_folio.text = "Folio ${lst[position].folio}"
        holder.tv_total.text = "Total ${lst[position].totalAmount}"
    }


    class HistoryViewHolder(itemView : View) : BaseViewHolder(itemView){
        val tv_table : TextView = itemView.findViewById(R.id.tv_table)
        val tv_folio : TextView = itemView.findViewById(R.id.tv_folio)
        val tv_total : TextView = itemView.findViewById(R.id.tv_total)
    }
}
