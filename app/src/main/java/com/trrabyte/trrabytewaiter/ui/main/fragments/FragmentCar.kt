package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.Dish
import com.trrabyte.trrabytewaiter.ui.main.MainActivity
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.utils.adapters.CarAdapter
import kotlinx.android.synthetic.main.menu_drawer_layout.*

class FragmentCar : BaseFragment<MainViewModel>(MainViewModel::class.java) {
    var car = HashMap<Int, Dish>()
    var removeList = ArrayList<Int>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.menu_drawer_layout, container, false)
        return v
    }

    override fun setObservers() {
        super.setObservers()
        fragmentViewModel?.getCar()?.observe(viewLifecycleOwner, Observer {
            setRecycler(it)
        })
        fragmentViewModel?.getCheck()?.observe(viewLifecycleOwner, Observer {
            tv_total.text = "Total $ ${String.format("%.2f", it)}"
        })
    }

    override fun setListeners() {
        super.setListeners()
        btn_agregar_car.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (fragmentViewModel?.isUpdate == false) {
            showProgress()
            fragmentViewModel?.sendOrder(ArrayList(car.values.toList()))
                ?.observe(viewLifecycleOwner,
                    Observer {
                        if (it.hasError) {
                            Utils.showSimpleDialog(requireContext(), message = it.message)
                        } else {
                            Utils.printBt(
                                requireContext(),
                                table = fragmentViewModel!!.getTable() ?: 1,
                                folio = fragmentViewModel!!.getFolio(),
                                products = ArrayList(car.values.toList()),
                                printPrice = false
                            )
                            startActivity(Intent(requireContext(), MainActivity::class.java))
                            requireActivity().finish()
                        }
                        hideProgress()
                    })

        } else {
//            UpdateCheck
            showProgress()
            fragmentViewModel?.updateOrder(removeList)?.observe(viewLifecycleOwner, Observer {
                if (it.hasError) {
                    Utils.showSimpleDialog(requireContext(), message = it.message)
                } else {
                    Utils.printBt(
                        requireContext(),
                        table = fragmentViewModel!!.getTable() ?: 1,
                        folio = fragmentViewModel!!.getFolio(),
                        products = ArrayList(fragmentViewModel!!.getUpdateMap().values.toList()),
                        printPrice = false
                    )
                    startActivity(Intent(requireContext(), MainActivity::class.java))
                    requireActivity().finish()
                }
                hideProgress()
            })
        }
    }

    private fun setRecycler(lst: HashMap<Int, Dish>) {
        car = lst
        val mainOrder = fragmentViewModel?.getMainOrder()
        val adapter = CarAdapter(requireContext(), ArrayList(car.values.toList()))
            .setDishChange {
                Log.v("tag", "para aqui")
                if (mainOrder != null && mainOrder.containsKey(it.productId) && mainOrder[it.productId]!!.count < it.count) {
                    val updateDish = it.clone()
                    updateDish.count = it.count - mainOrder[it.productId]!!.count
                    fragmentViewModel?.setUpdateHasMap(updateDish)
                } else {
                    val updateDish = it.clone()
                    fragmentViewModel?.setUpdateHasMap(updateDish)
                }
                car[it.productId]?.count = it.count
                fragmentViewModel?.setCar(car)
            }
            .setDishRemove {
                if (car.containsKey(it.id)) {
                    car.remove(it.id)
                    fragmentViewModel?.setCar(car)

                } else if (car.containsKey(it.productId)) {
                    car.remove(it.productId)
                    fragmentViewModel?.setCar(car)
                } else if (fragmentViewModel?.isUpdate!! && it.id!! > 0) {
                    removeList.add(it.id!!)
                }
                fragmentViewModel?.removeItemUpdate(it)
            }
            .setInitialCount(mainOrder)
        rv_car.layoutManager = LinearLayoutManager(requireContext())
        rv_car.adapter = adapter
    }
}