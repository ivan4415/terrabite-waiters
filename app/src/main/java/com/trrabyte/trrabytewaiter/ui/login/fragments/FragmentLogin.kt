package com.trrabyte.trrabytewaiter.ui.login.fragments

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.ApplicationBase
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.ui.login.data.LoginViewModel
import com.trrabyte.trrabytewaiter.ui.main.MainActivity
import com.trrabyte.trrabytewaiter.ui.recovery.RecoveryActivity
import com.trrabyte.trrabytewaiter.ui.stats.StatsActivity
import kotlinx.android.synthetic.main.fragment_login.*

class FragmentLogin : BaseFragment<LoginViewModel>(LoginViewModel::class.java){

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_login,container,false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationBase.getInstance().applicationContext)
        val token = preferences.getString("token","")
        val isAdmin = preferences.getBoolean("admin",false)
        if(!TextUtils.isEmpty(token)){
            if(isAdmin){
                requireActivity().startActivity(Intent(requireContext(), StatsActivity::class.java)
                    .putExtra("branchId",preferences.getInt("branchId",-1)))
                requireActivity().finish()
            }else {
                requireActivity().startActivity(Intent(requireContext(), MainActivity::class.java))
                requireActivity().finish()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_continue.setOnClickListener(this)
        btn_registrar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.btn_continue->{
                showProgress()
                fragmentViewModel?.doLogin(etCorreo.text.toString(),etPass.text.toString())?.observe(
                    viewLifecycleOwner, Observer {
                        if(it.hasError){
                            Utils.showSimpleDialog(requireContext(),message = it.message)
                        }else{
                            Utils.showToast("Success")
                            run loop@{
                                it.data?.credential?.user?.userRoleList?.forEach {userRole->
                                    if (userRole.role.id == 1 || userRole.role.id == 2) {
                                        requireActivity().startActivity(Intent(requireContext(), StatsActivity::class.java)
                                            .putExtra("branchId",it.data?.credential?.user?.branch?.id?:-1))
                                        requireActivity().finish()
                                        return@loop
                                    }
                                }
                                requireActivity().startActivity(Intent(requireContext(), MainActivity::class.java))
                                requireActivity().finish()
                            }
                        }
                        hideProgress()
                    }
                )
            }
            R.id.btn_registrar->{
                startActivity(Intent(requireContext(),RecoveryActivity::class.java))
            }
        }
    }
}