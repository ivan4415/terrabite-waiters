package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.NewBaseResponse
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.Dish
import com.trrabyte.trrabytewaiter.io.response.ProductCategory
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.utils.adapters.AdapterCategoryList
import com.trrabyte.trrabytewaiter.ui.utils.adapters.TablesAdapter
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.toolbar.*

class FragmentAdd : BaseFragment<MainViewModel>(MainViewModel::class.java) {
    private var tempCar: HashMap<Int, Dish>? = HashMap()
    private var folio = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        v = inflater.inflate(R.layout.fragment_add, container, false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if ( requireArguments().containsKey("id")) {
            getEditOrder(requireArguments().getInt("id"))
        } else {
            getMenu()
        }
    }

    override fun setListeners() {
        super.setListeners()
        tv_branch.visibility = View.VISIBLE
        tv_branch.text = "Ver orden"
        tv_branch.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.tv_branch->{
                drawer_layout.openDrawer(GravityCompat.END)
            }
        }
    }

    private fun getEditOrder(id : Int){
        showProgress()
        fragmentViewModel?.getEditOrder(id)?.observe(viewLifecycleOwner, Observer {
            if(it.hasError){
                Utils.showSimpleDialog(requireContext(),message = it.message)
            }else{
                setCategoryList(it.data?.productCategoryList!!)
                setTable()
                this.folio = it.data?.order?.folio?:-1
                fragmentViewModel?.setFolio(this.folio)
                vp_tables.currentItem = it.data?.order?.tableNumber!! -1
                vp_tables.visibility = View.GONE
                tv_table_number.text = "M-${it.data?.order?.tableNumber}"
                tv_table_number.visibility = View.VISIBLE
                it!!.data!!.unpaidList.forEach { dish ->
                    tempCar?.let {hasMap->
                        hasMap[dish.productId ?: -1] = dish.clone()
                        fragmentViewModel?.setMainOrder(dish)
                    }
                }
//                fragmentViewModel?.setMainOrder(tempCar?.clone() as HashMap<Int, Dish>? ?: HashMap())
                createCar()
                fragmentViewModel?.isUpdate = true
            }
            hideProgress()
        })
    }

    private fun createCar(){
//        lst.forEach{dish->
//            tempCar?.let {
//                it[dish.productId?:-1] = dish
//            }
//        }
        fragmentViewModel?.setCar(tempCar)
    }

    private fun getMenu() {
        showProgress()
        fragmentViewModel?.isUpdate = false
        fragmentViewModel?.getMenu()?.observe(viewLifecycleOwner, Observer {
            if (it.hasError) {
                Utils.showToast("Error")
            } else {
                folio = it.data?.folio ?: -1
                fragmentViewModel?.setFolio(folio)
                setTable()
                setCategoryList(it.data?.productCategoryList!!)
                if(requireArguments().getBoolean("isFromQR")){
                    vp_tables.currentItem = requireArguments().getInt("numOfTables") - 1
                    vp_tables.visibility = View.GONE
                    tv_table_number.text = "M-${requireArguments().getInt("numOfTables")}"
                    tv_table_number.visibility = View.VISIBLE
                }
            }
            hideProgress()
        })
        fragmentViewModel?.getCar()?.observe(viewLifecycleOwner, Observer {
            tempCar = it
        })
    }

    private fun setTable() {
        val tables = requireArguments().getSerializable("tables")
        val adapter = TablesAdapter(requireContext(), tables as ArrayList<NewBaseResponse>)
        vp_tables.adapter = adapter
        vp_tables.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                fragmentViewModel?.setTable(position + 1)
            }

            override fun onPageScrollStateChanged(state: Int) {}

        })
    }

    private fun setCategoryList(lst: ArrayList<ProductCategory>) {
        rv_categories.layoutManager = LinearLayoutManager(requireContext())
        rv_categories.adapter = AdapterCategoryList(requireContext(), lst)
            .setAddToCar { product, categoryId ->
                val dish: Dish = product.toDish(categoryId)
                if(!fragmentViewModel!!.isUpdate) {
                    dish.id?.let { id ->

                        tempCar = tempCar ?: HashMap()
                        tempCar?.let { map ->
                            if (map.containsKey(id)) {
                                map[id]!!.count += dish.count
                            } else {
                                map[id] = dish
                            }
                            fragmentViewModel?.setCar(map)
                        }
                    }
                }else{
//                    dish.productId = dish.id!!.toInt()
                    tempCar?.let {map->
                        if (map.containsKey(dish.id?:-1) && dish.id !=0) {
                            map[dish.id!!?:-1]!!.count += dish.count
                        } else {
                            val tempDish = dish.clone()
                            tempDish.id = 0
                            tempDish.productId = dish.id
                            map[dish.id!!?:-1] =tempDish
                        }
                        fragmentViewModel?.setCar(map)
                        val updateDish = dish.clone()
                        fragmentViewModel?.getMainOrder()?.let {
                            if(it.containsKey(dish.id)) {
                                map[dish.id]?.count?.let {mapCount->
                                    val tempCount =  mapCount - it[dish.id]!!.count
                                    updateDish.count = tempCount
                                }
                            }
                        }
                        fragmentViewModel?.setUpdateHasMap(updateDish)
                    }
                }
            }
        childFragmentManager
            .beginTransaction()
            .add(R.id.ll_drawerEnd, FragmentCar(), "")
            .disallowAddToBackStack()
            .commitAllowingStateLoss()
    }

}