package com.trrabyte.trrabytewaiter.ui.main.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.trrabyte.trrabytewaiter.R
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.io.response.Orders
import com.trrabyte.trrabytewaiter.ui.main.data.MainViewModel
import com.trrabyte.trrabytewaiter.ui.utils.adapters.AdapterHistory
import com.trrabyte.trrabytewaiter.ui.utils.dialogs.DatePickerFragment
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_history.drawer_layout
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.toolbar.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FragmentHistory : BaseFragment<MainViewModel>(MainViewModel::class.java) {

    val sdf = SimpleDateFormat("yyyy-MM-dd",Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?):
            View {
        v = inflater.inflate(R.layout.fragment_history,container,false)
        return v
    }

    override fun setObservers() {
        super.setObservers()
        getHistory(sdf.format(Calendar.getInstance().time))
        setFragmentObserver()
        setDrawerView()
    }

    override fun setListeners() {
        super.setListeners()
        ic_menu.setOnClickListener(this)
        ic_menu.visibility = View.VISIBLE
        img_toolbar.visibility = View.VISIBLE
        img_toolbar.setOnClickListener(this)
    }

    private fun getHistory(date : String){
        val sdf2 = SimpleDateFormat("dd/MM/yyyy",Locale.getDefault())
        val formatDate = sdf2.format(sdf.parse(date)?:"16/10/1990")

        tv_fecha.text = "Órdenes para el $formatDate"
        showProgress()
        fragmentViewModel?.getHistory(date)?.observe(viewLifecycleOwner,
            Observer {
                if(it.hasError){
                    Utils.showSimpleDialog(requireContext(),message = it.message)
                }else{
                    setHistory(it.data?.orders)
                }
                hideProgress()
            })
    }

    private fun setHistory(history : ArrayList<Orders>?){
        val adapter = AdapterHistory(requireContext(),history?: arrayListOf())
        rv_history.adapter = adapter
        rv_history.layoutManager = LinearLayoutManager(requireContext())
    }

    private val  requestPermissions = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if(isGranted){
            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.FRAGMENT_QR)
        }else{
            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
        }
    }

    private fun setFragmentObserver(){
        fragmentViewModel?.getFragment()?.observe(viewLifecycleOwner, Observer {
            when(it?: MainViewModel.FragmentsMenu.DEFAULT){
                MainViewModel.FragmentsMenu.FRAGMENT_ORDERS -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                    navigateToFragment(R.id.history_to_main)
                    fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                }
                MainViewModel.FragmentsMenu.FRAGMENT_QR -> {
                    when {
                        ContextCompat.checkSelfPermission(requireContext(),
                            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED -> {
                            drawer_layout.closeDrawer(GravityCompat.START)
                            navigateToFragment(R.id.history_to_qr)
                            fragmentViewModel?.setFragment(MainViewModel.FragmentsMenu.DEFAULT)
                        }
//                        shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> {
//                            Utils.showSimpleDialog(requireContext(),message = getString(R.string.camara_permission)
//                                ,positiveTextButton = getString(R.string.aceptar),positiveListener ={_,_->
//                                    val i = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//                                    i.data = Uri.fromParts("package","com.trrabyte.trrabytewaiter",null)
//                                    startActivity(i)
//                                },negativeText = getString(R.string.cancelar),
//                                negativeListener = DialogInterface.OnClickListener{d,_->
//                                    d.dismiss()
//                                }
//                            )
//                        }
                        else -> {
                            requestPermissions.launch(Manifest.permission.CAMERA)
//                            requestPermissions( arrayOf(Manifest.permission.CAMERA),PERMISSION_RESULT)
                        }
                    }
                }
                MainViewModel.FragmentsMenu.FRAGMENT_HISTORY -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
                MainViewModel.FragmentsMenu.DEFAULT -> {
                    drawer_layout.closeDrawer(GravityCompat.START)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.ic_menu->{
                drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.img_toolbar->{
                DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    getHistory("$year-${Utils.getCeroDateSting(month+1)}-${Utils.getCeroDateSting(dayOfMonth)}")
                }).show(childFragmentManager,"datePicker")
            }
        }
    }

    private fun setDrawerView() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.ll_drawer_start, FragmentMenu(), "")
            .disallowAddToBackStack()
            .commitAllowingStateLoss()
    }

}