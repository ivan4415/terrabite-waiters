package com.trrabyte.trrabytewaiter.ui.utils.adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.ui.utils.BaseAdapter
import com.trrabyte.trrabytewaiter.io.response.Dish

class CarAdapter (private val ctx: Context, lst : ArrayList<Dish>): BaseAdapter<CarAdapter.CarViewHolder,Dish>(lst){

    private var dishChange : ((Dish)->Unit)? = null
    private var dishRemove : ((Dish)->Unit)? = null
    private var initialCount : HashMap<Int,Dish>? = HashMap()

    fun setDishChange(lmda : (Dish)->Unit):CarAdapter{
        dishChange = lmda
        return this
    }

    fun setDishRemove(lmda: (Dish) -> Unit):CarAdapter{
        dishRemove = lmda
        return this
    }

    fun setInitialCount(mainOrder : HashMap<Int,Dish>?) : CarAdapter{
        initialCount = HashMap(mainOrder)
        return this
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        return CarViewHolder(LayoutInflater.from(ctx).inflate(R.layout.item_menu,parent,false))
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val lst2  = ArrayList(lst)
        var initialDish :Dish? = initialCount?.let {   initialCount!![lst[position].productId] }
        holder.tv_nombre.text = if(!TextUtils.isEmpty(lst[position].name)) lst[position].name else lst[position].productName
        holder.tv_cantidad.text = lst[position].count.toString()
        holder.tv_cost.text = "$ ${String.format("%.2f",lst[position].price * lst[position].count)}"
        initialDish?.let {
            holder.img_trash.visibility = View.GONE
            if(lst[position].count > it.count  && lst[position].count>=2){
                holder.img_subst.visibility = View.VISIBLE
            }else{
                holder.img_subst.visibility = View.GONE
            }
        } ?: let{
            if (lst[position].count >= 2) {
                holder.img_subst.visibility = View.VISIBLE
                holder.img_trash.visibility = View.GONE
            } else {
                holder.img_subst.visibility = View.GONE
                holder.img_trash.visibility = View.VISIBLE
            }
        }

        holder.img_add.setOnClickListener{
            val dish = lst2[position].clone()
            dish.count +=1
            holder.tv_cantidad.text = dish.count.toString()
            holder.tv_cost.text = "$ ${String.format("%.2f",dish.price * dish.count)}"
            dishChange?.invoke(dish)
//            lst2[position].count += 1
//            holder.tv_cantidad.text = lst2[position].count.toString()
//            holder.tv_cost.text = "$ ${String.format("%.2f",lst2[position].price * lst2[position].count)}"
//            dishChange?.invoke(lst2[position])
        }

        holder.img_subst.setOnClickListener{
            val dish = lst2[position].clone()
            dish.count -=1
            holder.tv_cantidad.text = dish.count.toString()
            holder.tv_cost.text = "$ ${String.format("%.2f",dish.price * dish.count)}"
            dishChange?.invoke(dish)
//            lst[position].count -= 1
//            holder.tv_cantidad.text = lst[position].count.toString()
//            holder.tv_cost.text = "$ ${String.format("%.2f",lst[position].price * lst[position].count)}"
//            dishChange?.invoke(lst[position])
        }

        holder.img_trash.setOnClickListener{
            dishRemove?.invoke(lst[position])
            lst.remove(lst[position])
            notifyDataSetChanged()
        }

        if(!TextUtils.isEmpty(lst[position].imageUrl)) {
            Picasso.get().load(lst[position].imageUrl)
                .placeholder(R.drawable.logo)
                .error(R.drawable.logo)
                .into(holder.img_product)
        }
    }

    class CarViewHolder(itemView : View) : BaseAdapter.BaseViewHolder(itemView){
        val tv_nombre : TextView = itemView.findViewById(R.id.tv_nombre)
        val tv_cantidad : TextView = itemView.findViewById(R.id.tv_cantidad)
        val tv_cost : TextView = itemView.findViewById(R.id.tv_cost)
        val img_product : ImageView = itemView.findViewById(R.id.img_product)
        var img_add : ImageView = itemView.findViewById(R.id.img_add)
        var img_subst : ImageView = itemView.findViewById(R.id.img_subst)
        var img_trash : ImageView = itemView.findViewById(R.id.img_trash)
    }

    companion object {
        private val lst2 = arrayListOf<Dish>()
    }
}
