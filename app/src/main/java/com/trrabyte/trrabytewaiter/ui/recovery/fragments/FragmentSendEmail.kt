package com.trrabyte.trrabytewaiter.ui.recovery.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.trrabyte.trrabytewaiter.R
import com.trrabyte.trrabytewaiter.base.io.Utils
import com.trrabyte.trrabytewaiter.base.ui.BaseFragment
import com.trrabyte.trrabytewaiter.ui.recovery.data.RecoveryViewModel
import kotlinx.android.synthetic.main.fragment_recovery_email.*

class FragmentSendEmail : BaseFragment<RecoveryViewModel>(RecoveryViewModel::class.java){

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container,savedInstanceState)
        v = inflater.inflate(R.layout.fragment_recovery_email,container,false)
        return v
    }

    override fun setListeners() {
        super.setListeners()
         btn_continue.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.btn_continue->{
                showProgress()
                fragmentViewModel?.sendEmail(et_email.text.toString())?.observe(viewLifecycleOwner,
                    Observer {
                        if(it.hasError){
                            Utils.showSimpleDialog(requireContext(),message = it.message)
                        }else{
                            navigateToFragment(R.id.to_send_code)
                        }
                        hideProgress()
                    })
            }
        }
    }
}