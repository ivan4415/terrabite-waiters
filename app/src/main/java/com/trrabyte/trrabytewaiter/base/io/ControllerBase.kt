package com.trrabyte.trrabytewaiter.base.io

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class ControllerBase(var context: Context) {
    protected lateinit var retrofit: Retrofit

    var interceptor = HttpLoggingInterceptor()


    constructor(context : Context, url : String):this(context){
        this.context = context
        retrofit = getClientRetrofit(url)
    }

    private fun getClientHttp (): OkHttpClient.Builder{
        interceptor.level = (HttpLoggingInterceptor.Level.BODY)
        var customInterceptor =
            CustomInterceptor()
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .addInterceptor(customInterceptor)

//        val sslContext = Utils.createCertificate(context.resources.openRawResource(R.raw.puratos_cer))
//        httpClient.sslSocketFactory(sslContext.socketFactory,systemDefaultTrustManager())

        httpClient.hostnameVerifier { _, _ -> true }
        //TODO comentar en Release
        httpClient.addInterceptor(interceptor)
        httpClient.addNetworkInterceptor(StethoInterceptor())
        return httpClient
    }

//    private fun systemDefaultTrustManager(): X509TrustManager {
//        try {
//            val trustManagerFactory =
//                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
//            trustManagerFactory.init(null as KeyStore?)
//            val trustManagers = trustManagerFactory.getTrustManagers()
//            check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
//                "Unexpected default trust managers:" + Arrays.toString(
//                    trustManagers
//                )
//            }
//            return trustManagers[0] as X509TrustManager
//        } catch (e: GeneralSecurityException) {
//            throw AssertionError() // The system has no TLS. Just give up.
//        }
//    }

    fun getClientRetrofit(url : String): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(getUrlBaseFormat(url))
            .client(getClientHttp().build())
            .build()
    }

    fun getUrlBaseFormat(url : String):String{
        return if(url.endsWith("/")){
            url
        }else{
            "$url/"
        }
    }

}