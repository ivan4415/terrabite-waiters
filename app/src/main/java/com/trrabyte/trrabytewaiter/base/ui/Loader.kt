package com.trrabyte.trrabytewaiter.base.ui

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.trrabyte.trrabytewaiter.R
//import kotlinx.android.synthetic.main.loader.*
import kotlinx.coroutines.*
import kotlin.coroutines.coroutineContext

class Loader : DialogFragment() {

    var v : View? = null
    lateinit var img_load : ImageView
    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = requireActivity().window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.transparent_d )
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        v = LayoutInflater.from(requireContext()).inflate(R.layout.loader,null,false)
        isCancelable = false
//        img_load = v!!.findViewById(R.id.img_load)

        val builder = AlertDialog.Builder(requireContext(), R.style.transparentDialog)
        builder.setView(v)
            .setCancelable(false)
        val customDialog = builder.create()

        customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        customDialog.window?.decorView?.systemUiVisibility= View.SYSTEM_UI_FLAG_FULLSCREEN
        return customDialog!!
    }

    override fun onResume() {
        super.onResume()
        startAnimation()
    }

    override fun dismiss() {
        scope.coroutineContext.cancelChildren()
        super.dismiss()
    }

    override fun onPause() {
        super.onPause()
        scope.coroutineContext.cancelChildren()
    }

    private fun startAnimation(){
//        GlobalScope.launch {
//            withContext(Dispatchers.Main) {
//        scope.launch {
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l00
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l01
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l02
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l03
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l04
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l05
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l06
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l07
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l08
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l09
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l10
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l11
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l12
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l13
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l14
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l15
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l16
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l17
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l18
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l19
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l20
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l21
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l22
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l23
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l24
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l25
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l26
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l27
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l28
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l29
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l30
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l31
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l32
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l33
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l34
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l35
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l36
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l37
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l38
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l39
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l40
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l41
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l42
//                    )
//                )
//                delay(200)
//                img_load.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.l43
//                    )
//                )
//                delay(200)
//                startAnimation()
////            }
//        }
    }
}