package com.trrabyte.trrabytewaiter.base.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.BaseResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class BaseRepository  {

    private val error = MutableLiveData<String?>()

    fun setErrorMessage(message : String?)  {
        error.value = message
    }

    fun getErrorMessage() : LiveData<String?> {
        return error
    }

    fun <Ob,T : BaseResponse<Ob>> Call<T>.service(res : MutableLiveData<BaseResponse<Ob>>){
        enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                response.body()?.run{
                    when(codigoOperacion){
                        BaseResponse.SUCCESS ->{
                            res.value = this
                        }
                        else->{
                            res.value = BaseResponse.setResponseError(message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                res.value = BaseResponse.setResponseError(t.message)
            }

        })
    }

}