package com.trrabyte.trrabytewaiter.base.io

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.Serializable

abstract class BaseRequest<T : BaseRequest<T>> : Serializable {

    val map = HashMap<String,RequestBody>()
    var delegate = FormDataDelegate<T>()
    var arraydelegate =
        ArrayFormDataDelegate<T>()
    var locationdelegate =
        LocationFormDataDelegate<T>()
    var filedelegate = FileFormDataDelegate<T>()
    private var values = HashMap<String,String>()
    var file : File? = null
    var files = MultipartBody.Builder()

    fun addString(key : String,value : String?): String?{
        value?.run {
            map[key] = RequestBody.create(MediaType.parse("multipart/form-data"), this)
            values[key] = this
        }
        return value
    }

    fun addFile(key : String, file : File?){
        file?.run{
            val body = RequestBody.create(MediaType.parse("application/octet-stream"),file)
            map["$key\"; filename=\"" + file.name] = body

            this@BaseRequest.file = file
        }
    }

    fun getValue(key : String) : String?{
        return values[key]
    }
}
