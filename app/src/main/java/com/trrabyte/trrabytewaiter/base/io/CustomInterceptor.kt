package com.trrabyte.trrabytewaiter.base.io

import android.preference.PreferenceManager
import com.trrabyte.trrabytewaiter.base.io.ApplicationBase
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject
import java.lang.Exception

class CustomInterceptor : Interceptor {
    private val JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8")
    override fun intercept(chain: Interceptor.Chain): Response {
        val preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationBase.getInstance().applicationContext)
        val token = preferences.getString("token","")
        val request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()

        var response : Response = chain.proceed(request)
        if(response.code()!=200){
            val string = try{
                JSONObject(response.body()?.string()?:"{responseCode:900,message:'Error al convertir la cadena a objeto'}")
            }catch (e : Exception){
                JSONObject("{responseCode:901,message:'Empty body'}")
            }
            response = response.newBuilder()
                .code(200)
                .body(ResponseBody.create(JSON_MEDIA_TYPE,string.toString()))
                .build()
        }

        return  response
    }
}