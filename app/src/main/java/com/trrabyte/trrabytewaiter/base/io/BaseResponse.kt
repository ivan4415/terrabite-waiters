package com.trrabyte.trrabytewaiter.base.io

import com.google.gson.annotations.SerializedName

open class BaseResponse<T> {

    @SerializedName("responseCode")
    var responseCode : Int? = null

    @SerializedName("message")
    var message : String? = null

    @SerializedName("data")
    var data : T? = null

    var hasError : Boolean = false

    var codigoOperacion : Int? = null
    get() {
        return if(responseCode == 200 || responseCode == 201){
            SUCCESS
        }else {
            responseCode
        }
    }

    companion object{
        fun <T>setResponseError(message : String?) : BaseResponse<T> {
            val responseError = BaseResponse<T>()
            responseError.hasError = true
            responseError.message = message ?: ""
            return responseError
        }
        const val SUCCESS = 200
    }
}