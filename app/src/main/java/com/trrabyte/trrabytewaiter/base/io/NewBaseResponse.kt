package com.trrabyte.trrabytewaiter.base.io

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class NewBaseResponse : Serializable {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = ""
}