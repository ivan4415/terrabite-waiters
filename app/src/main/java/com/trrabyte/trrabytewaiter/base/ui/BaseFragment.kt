package com.trrabyte.trrabytewaiter.base.ui

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.trrabyte.trrabytewaiter.base.io.Utils

open class BaseFragment<T : BaseViewModel>(var viewModel: Class<T>) : Fragment(),View.OnClickListener {

    var fragmentViewModel : T? = null
    var loader : Loader? = null
    lateinit var v : View

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setViewModel()
        setObservers()
        setListeners()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            100->{
                Utils.getLocation(
                    this,
                    fragmentViewModel!!.locationret
                )
            }
        }
    }

    private fun setViewModel() : T?{
        fragmentViewModel = ViewModelProvider(requireActivity()).get(viewModel)
        return fragmentViewModel
    }

    fun navigateToFragment(@IdRes idFragment : Int, data : Bundle = Bundle()){
        (activity as BaseActivity<*>).navigateToFragment(idFragment,data)
    }

    open fun setObservers(){
        fragmentViewModel?.getError()?.observe(viewLifecycleOwner, Observer {
            Utils.showSimpleDialog(requireContext(),message = it)
        })
        fragmentViewModel?.progressBar?.observe(viewLifecycleOwner, Observer{
            if(it == false) hideProgress() else showProgress()
        })

    }

    open fun setListeners(){

    }
    override fun onClick(v: View?) {

    }

    fun showProgress(){
        loader?.run { dismiss()}
        loader = Loader()
        loader?.show(parentFragmentManager,"SHOW")
    }

    fun hideProgress(){
        loader?.run {
            dismiss()
        }
    }

}