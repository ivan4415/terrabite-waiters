package com.trrabyte.trrabytewaiter.base.ui

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.trrabyte.trrabytewaiter.R
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity<VM : BaseViewModel>(@NavigationRes var nav : Int) : AppCompatActivity() {

    var viewModel: VM? = null
    var navHost: NavController? = null
    var navController: NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navHost = findNavController(R.id.navHost)
        navController = Navigation.findNavController(this, R.id.navHost)
        setNavGraph()
//        setDrawer(false)
    }

    private fun setNavGraph(){
        navHost?.navInflater?.inflate(nav)?.let { navController?.graph =it}
    }

    fun navigateToFragment(@IdRes id: Int, bundle: Bundle) {
        if (navHost?.currentDestination?.getAction(id) != null) {
            navHost?.navigate(id, bundle)
        }
    }

//    fun setDrawer(setDrawer : Boolean) {
//        if (setDrawer) {
//            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
//            val mDrawerToggle = object : ActionBarDrawerToggle(
//                this,
//                drawer_layout,
//                R.string.nav_app_bar_open_drawer_description,
//                R.string.nav_app_bar_open_drawer_description
//            ) {}
//            drawer_layout.addDrawerListener(mDrawerToggle)
//            mDrawerToggle.syncState()
//        } else {
//            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
//        }
//    }
}