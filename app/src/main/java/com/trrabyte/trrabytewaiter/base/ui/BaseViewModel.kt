package com.trrabyte.trrabytewaiter.base.ui

import android.app.Application
import android.location.Location
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trrabyte.trrabytewaiter.base.io.Utils


open class BaseViewModel(var activity: Application) : AndroidViewModel(activity) {

    val locationret = MutableLiveData<Location>()
    val repo: BaseRepository by lazy { BaseRepository() }
    val progressBar = MutableLiveData<Boolean>()

    fun getLastLocation(fragment: Fragment): LiveData<Location> {
        Utils.getLocation(fragment, locationret)
        return locationret
    }

    fun getError() : LiveData<String?>{
        return repo.getErrorMessage()
    }

    fun setProgress(isVisible : Boolean){
        progressBar.postValue(isVisible)
    }

}