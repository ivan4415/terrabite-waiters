package com.trrabyte.trrabytewaiter.base.io

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Looper
import android.text.TextUtils
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.dantsu.escposprinter.EscPosPrinter
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.dantsu.escposprinter.textparser.PrinterTextParserImg
import com.trrabyte.trrabytewaiter.R

import com.google.android.gms.location.*
import com.google.android.material.textfield.TextInputLayout
import com.trrabyte.trrabytewaiter.io.response.Dish
import org.w3c.dom.Text
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

object Utils {

    val IMAGE_RESULT = 0x0001

    fun showSimpleDialog(
        ctx: Context,
        title: String? = null,
        message: String? = null,
        positiveTextButton: String? = null,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeText: String? = null,
        negativeListener: DialogInterface.OnClickListener? = null
    ) {
        val dialog = AlertDialog.Builder(ctx)
            .setTitle(title ?: "Terrabite")
            .setMessage(message ?: "Algo salió mal.")
            .setPositiveButton(
                positiveTextButton ?: "Aceptar",
                positiveListener
                    ?: DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() })
        negativeListener?.let {
            dialog.setNegativeButton(negativeText ?: "Cancelar", negativeListener)
        }
        dialog.show()
    }

    fun showToast(message: String) {
        Toast.makeText(ApplicationBase.getInstance(), message, Toast.LENGTH_SHORT).show()
    }

    fun validateData(
        edtText: ArrayList<EditText>, ti: ArrayList<TextInputLayout>,
        errors: ArrayList<String>
    ): Boolean {
        var i = 0
        edtText.forEach {
            if (TextUtils.isEmpty(it.text)) {
                ti[i].error = errors[i]
                return false
            } else {
                ti[i].error = null
            }
            i += 1
        }
        return true
    }

    fun openGallery(fragment: Fragment) {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        fragment.startActivityForResult(
            intent,
            IMAGE_RESULT
        )
    }

    fun getLocation(fragment: Fragment, locationret: MutableLiveData<Location>) {
//        var mFusedLocationClient: FusedLocationProviderClient
//        var mLocationCallback: LocationCallback
//        val ctx = fragment.requireContext()
//        val permissons = arrayOf(
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.ACCESS_COARSE_LOCATION
//        )
//        if (ContextCompat.checkSelfPermission(
//                ctx,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) == PackageManager.PERMISSION_GRANTED
//        ) {
//            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
//            mFusedLocationClient.lastLocation.addOnCompleteListener(fragment.requireActivity()) { task ->
//                val location: Location? = task.result
//                if (location == null || location.latitude == 0.0 || location.longitude == 0.0) {
//                    val mLocationRequest = LocationRequest()
//                    mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//                    mLocationRequest.interval = 10000
//                    mLocationRequest.fastestInterval = 5000
//                    mLocationCallback = object : LocationCallback() {
//                        override fun onLocationResult(locationResult: LocationResult) {
//                            locationret.value = locationResult.lastLocation
//                        }
//                    }
//                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
//                    mFusedLocationClient.requestLocationUpdates(
//                        mLocationRequest, mLocationCallback,
//                        Looper.myLooper()
//                    )
//                } else {
//                    locationret.value = location
//                }
//            }
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                fragment.requestPermissions(permissons, 100)
//            }
//        }
    }

//    fun getCard(){
//        getTokenOpenPay("UserName",
//                "1234567890987654321",9,23,
//                "123"){error,token,enum->
//            if(error){
//                showToast("Tiene el error: ${enum.toString()}")
//            }else{
//                showToast("Se pudo tokenizar: $token")
//            }
//        }
//    }
//    /**
//     *Método para tokenizar las tarjetas con OpenPay
//     * @param name Nombre que aparece en la tarjeta.
//     * @param cardNumber Numero de la tarjeta.
//     * @param exMonth Mes en el que expira la tarjeta en entero (1-12) ej: 9
//     * @param exYear Año en el que expira la tarjeta en entero a 2 digitos ej : 22
//     * @param cvv2 codigo de validación a tres digitos, es el codigio verificador de la tarjeta.
//     * @param success lamda que regresa el valor del token o codigo de error, contiene 3 parametros
//     * @sample Utils.getCard()
//     * */
//    fun getTokenOpenPay(name : String,cardNumber : String, exMonth : Int,
//                        exYear : Int, cvv2 : String,success : (error : Boolean,token : Token?,message : String)->Unit){
//        if(!CardValidator.validateNumber(cardNumber)){
//            success.invoke(true,null,"Tarjeta no valida")
//            return
//        }
//        val card = Card()
//        card.holderName(name)
//            .cardNumber(cardNumber)
//            .expirationMonth(exMonth)
//            .expirationYear(exYear)
//            .cvv2(cvv2)
//
//        ApplicationBase.getInstance().openPayController.createToken(card,object : OperationCallBack<Token>{
//            override fun onSuccess(p0: OperationResult<Token>?) {
//                success.invoke( p0?.result==null ,p0?.result,"Success")
//            }
//
//            override fun onCommunicationError(p0: ServiceUnavailableException?) {
//                success.invoke(true,null,p0?.message?:"Error de conexión.")
//            }
//
//            override fun onError(p0: OpenpayServiceException?) {
//                when(p0?.errorCode){
//                    3001->success.invoke(true,null,"Tarjeta declinada.")
//                    3002->success.invoke(true,null,"Tarjeta expirada.")
//                    3003->success.invoke(true,null,"Saldo insuficiente.")
//                    3004->success.invoke(true,null,"Tarjeta con reporte de robo")
//                    3005->success.invoke(true,null,"Sospecha de fraude")
//                    2002->success.invoke(true,null,"La tarjeta ya existe")
//                    else->success.invoke(true,null,"Error generando la tarjeta")
//
//                }
//            }
//
//        })
//    }

    fun getDeviceId(activity: Activity): String {
        return ApplicationBase.getInstance().getDeviceId(activity)
    }

    fun validatePass(pass: String): Map<Boolean, String> {
        val special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]")
        if (pass.length < 8) {
            println("La contraseña es menor a 8")
            return mapOf(Pair(false, "La contraseña es menor a 8"))
        } else if (!pass.matches(".*[A-Z].*".toRegex())) {
            println("La contraseña no contiene una mayuscula")
            return mapOf(Pair(false, "La contraseña no contiene una mayusucla"))
        } else if (!special.matcher(pass).find()) {
            println("La contraseña no contiene un caracter especial")
            return mapOf(Pair(false, "La contraseña no contiene un caracter especial"))
        } else if (!pass.matches(".*[0-9].*".toRegex())) {
            println("La contraseña no contiene numeros")
            return mapOf(Pair(false, "La contraseña no contiene números"))
        }
        println("La contraseña es correcta")
        return mapOf(Pair(true, "La contraseña es correct"))
    }

    fun Button.setLambda(value: (Boolean) -> Unit) {

    }

    @Throws(Exception::class)
    fun getFirstPrinter() : EscPosPrinter{
        try {
            return EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(),203,58f,32)
        }catch (e : Exception){
            throw Exception("error en la impresión.")
        }
    }

    fun printBt(
        context: Context, table: Int = 0,
        folio: Int = 0, products: ArrayList<Dish> = arrayListOf(),printPrice: Boolean = true
    ) {
        val printers = BluetoothPrintersConnections()
        printers.list?.let {
            if (it.isNotEmpty()) {
                val printer = try {
                    getFirstPrinter()
                }catch (e : Exception){
                    printBt(context,table, folio, products)
                    return@printBt
                }
                showToast("Imprimiendo...")
                printer.printFormattedText(
                    createTicket(
//                        "<img>" + PrinterTextParserImg.bitmapToHexadecimalString(
//                            printer,
//                            context.resources.getDrawableForDensity(
//                                R.drawable.logo,
//                                DisplayMetrics.DENSITY_MEDIUM
//                            )
//                        ) + "</img>\n"
                                            "", table, folio, products,printPrice
                    )
                )
                showToast("Finalizado.")
                printer.disconnectPrinter()
            } else {
                showSimpleDialog(context,
                    message = "Necesitas configurar tu impresora antes de imprimir",
                    positiveTextButton = "Configurar",
                    positiveListener = DialogInterface.OnClickListener { d, _ ->
                        context.startActivity(Intent().setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS))
                    },
                    negativeText = "Cancelar",
                    negativeListener = DialogInterface.OnClickListener { d, _ ->
                        d.dismiss()
                    })
            }
        } ?: showSimpleDialog(context,
            message = "Necesitas configurar tu impresora antes de imprimir",
            positiveTextButton = "Configurar",
            positiveListener = DialogInterface.OnClickListener { d, _ ->
                context.startActivity(Intent().setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS))
            },
            negativeText = "Cancelar",
            negativeListener = DialogInterface.OnClickListener { d, _ ->
                d.dismiss()
            })
    }

    fun createTicket(
        ticket: String,
        table: Int = 0,
        folio: Int = 0,
        products: ArrayList<Dish>,
        printPrice : Boolean = true
    ): String {
        val date = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SimpleDateFormat("dd/MM/YYYY hh:mm").format(Calendar.getInstance().time)
        } else {
            SimpleDateFormat("dd/MM hh:mm").format(Calendar.getInstance().time)
        }
        var ticketp = StringBuilder()
        ticketp.append(ticket)
        ticketp.append(
            "\n\n"+
            "[L]Folio: ${folio}\n" +
                    "[L]Mesa: ${table}\n"+
                    "[L]Fecha: ${date}\n"+
                    "[L]\n" +
                    "[C]================================\n" +
                    (if(printPrice)"[C]Producto [C]Cant. [C]Prec. [C]Subtotal\n" else "[C]Producto [R]Cant. \n")+
                    "[C]================================\n"
        )
        var total = 0.0
        products.forEach {
            it.name?.let { name ->
                var testName = if(TextUtils.isEmpty(name)) it.productName ?: "" else name
                if(printPrice) {
                    while (testName.length > 9) {
//                    println("[C]${testName.substring(0,8).trim()}")
                        ticketp.append("[L]${testName.substring(0, 9).trim()}\n")
                        testName = testName.substring(9, testName.length)
                    }
//                println(testName)
                    total += it.subTotalAmount!!
                    ticketp.append(
                        "[C]${testName} [C]${it.count} [R]${
                            String.format(
                                "%.2f",
                                it.price
                            )
                        } [R]${String.format("%.2f", it.subTotalAmount)}\n"
                    )
                }else{
                    while (testName.length > 25) {
//                    println("[C]${testName.substring(0,8).trim()}")
                        ticketp.append("[L]${testName.substring(0, 25).trim()}\n")
                        testName = testName.substring(25, testName.length)
                    }
//                println(testName)
//                    total += it.subTotalAmount!!
                    ticketp.append(
                        "[L]${testName} [R]${it.count} \n"
                    )
                }
                ticketp.append("\n")
            }
        }
        ticketp.append("\n")
        if(printPrice)ticketp.append("[R]__________\n")
        if(printPrice)ticketp.append("[R]Total ${String.format("%.2f", total)}")
        ticketp.append("\n\n")
        ticketp.append("................................")
        return ticketp.toString()
    }

    fun getCeroDateSting(date : Int):String{
        return if(date<10){
            "0$date"
        }else{
            date.toString()
        }
    }
}