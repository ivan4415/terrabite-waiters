package com.trrabyte.trrabytewaiter.base.ui.utils

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH : BaseAdapter.BaseViewHolder,Item> (var lst : ArrayList<Item>)
    : RecyclerView.Adapter<VH>(){

    var itemClickListener : ((Item)->Unit)?= null
    var itemLongClickListener : ((Item)->Unit)? = null
    private lateinit var v : View

    fun setOnItemClicListener(mCallback : ((Item)->Unit)): BaseAdapter<VH, Item> {
        this.itemClickListener = mCallback
        return this
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    fun setOnItemLongClickListner(mCallback: (Item) -> Unit){
        this.itemLongClickListener = mCallback
    }

    abstract class BaseViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)
}

