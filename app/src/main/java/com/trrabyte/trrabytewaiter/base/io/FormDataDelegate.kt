package com.trrabyte.trrabytewaiter.base.io

import com.google.android.gms.maps.model.Marker
import java.io.File
import kotlin.reflect.KProperty


class FormDataDelegate<T : BaseRequest<T>> {

    operator fun getValue(request: T, property: KProperty<*>): String {
        return request.getValue(property.name)?:""
    }

    operator fun setValue(request : T, property: KProperty<*>, s: String?) {
        request.addString(property.name,s)
    }

}

class ArrayFormDataDelegate<T : BaseRequest<T>>{

    operator fun getValue(request: T, property: KProperty<*>): ArrayList<String> {
//        return request.getValue(property.name)?:""
        return arrayListOf<String>()
    }

    operator fun setValue(request : T, property: KProperty<*>, s: ArrayList<String>?) {
        var i = 0
        s?.forEach {
            request.addString("${property.name}[$i]", it)
            i++
        }
    }
}

class LocationFormDataDelegate<T : BaseRequest<T>>{
    operator fun getValue(request: T, property: KProperty<*>): ArrayList<Marker> {
//        return request.getValue(property.name)?:""
        return arrayListOf<Marker>()
    }

    operator fun setValue(request : T, property: KProperty<*>, s: ArrayList<Marker>?) {
        var i = 0
        s?.forEach {
            request.addString("${property.name}[$i].latitude", it.position.latitude.toString())
            request.addString("${property.name}[$i].longitude",it.position.longitude.toString())
            i++
        }
    }
}

class FileFormDataDelegate<T : BaseRequest<T>>{
    operator fun getValue(request : T ,property: KProperty<*>): File?{
        return request.file
    }
    operator fun setValue(request: T,property: KProperty<*>,f : File?){
        request.addFile(property.name,f)
    }
}