package com.trrabyte.trrabytewaiter.base.io

import android.app.Activity
import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
//import mx.openpay.android.Openpay

open class ApplicationBase : Application() {

    lateinit var ctx: Context
//    lateinit var openPayController: Openpay

    private var openPayDeviceId: String? = null

    companion object {

        private var instance = ApplicationBase()

        fun getInstance(): ApplicationBase {
            return instance
        }
    }

    fun getDeviceId(actitivity : Activity): String {
//        return openPayDeviceId ?: openPayController.deviceCollectorDefaultImpl.setup(actitivity)
        return ""
    }

    override fun onCreate() {
        super.onCreate()
        initApplication()
    }

    open fun initApplication() {
        instance = this
        ctx = applicationContext
        Stetho.initializeWithDefaults(this)
//        openPayController = Openpay(BuildConfig.MERCHANT_ID, BuildConfig.OPEN_KEY, false)
//        openPayDeviceId = openPayController.deviceCollectorDefaultImpl.setup(this)
    }

    open fun createDataBase(){

    }

    protected fun testProtected(){

    }

}